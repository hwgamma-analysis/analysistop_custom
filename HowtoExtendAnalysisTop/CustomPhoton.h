#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMPHOTON_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMPHOTON_H

#include "TopObjectSelectionTools/IsolationTools.h"

#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>

// isolation tool
#include <IsolationSelection/IIsolationSelectionTool.h>

namespace top{
    class CustomPhoton{
  
    public:
      CustomPhoton(TString Iso);
      
      virtual ~CustomPhoton(){}

      int isIsoTight(const xAOD::Photon& pho) const;
      bool passLoosePrime(const xAOD::Photon& pho,int cutNumber) const;
      int truthTypeCondensed(int truType,int truOrigin) const;

    protected:
      asg::AnaToolHandle<CP::IIsolationSelectionTool> m_photonIsolationTool;

  };


}

#endif
