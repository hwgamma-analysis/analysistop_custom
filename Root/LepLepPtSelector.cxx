/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/LepLepPtSelector.h"

namespace top{

  LepLepPtSelector::LepLepPtSelector(const std::string& params) :
    SignValueSelector("LepLepPt", params) {
  }

    //If the event number divided by two has no remainder then return true.
  bool LepLepPtSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t closPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;

    for(const xAOD::Electron* e : event.m_electrons) {
        float dR = leaLep_p4.DeltaR(e->p4());
        if (dR<0.4) continue; // so it is not the same particle
        if(dR<closPho_dR){
            closPho_p4 = e->p4();
            closPho_dR=dR;
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        float dR = leaLep_p4.DeltaR(mu->p4());
        if (dR<0.4) continue; // so it is not the same particle
        if(dR<closPho_dR){
            closPho_p4 = mu->p4();
            closPho_dR=dR;
        }
    }


    float m_LepLepPt = leaLep_p4.Pt()+closPho_p4.Pt();
    
    return checkFloat(m_LepLepPt, value());
  }

  //For the cutflow and terminal output
  std::string LepLepPtSelector::name() const {
      return "LEPLEPPt";
  }
}
