/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMMUON_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMMUON_H

//#include "TopObjectSelectionTools/MuonSelectionBase.h"
#include "TopObjectSelectionTools/IsolationTools.h"

#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>
#include "MuonAnalysisInterfaces/IMuonSelectionTool.h"
// isolation tool
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "TruthClassification/TruthClassificationTool.h"


namespace top{
  /**
   * @brief Select only muons in the positive eta direction
   *        No physics reason, demonstration of a plug-in
   */
  
  //class CustomMuon :  public MuonSelectionBase {
    class CustomMuon{
  
    public:
      CustomMuon(int PIDcut,TString Iso);
      
      virtual ~CustomMuon(){}
      /**
      * @brief Implements the logic to select good muons.
      *
      * @param mu The muon that we want to check.
      * @return True if the muon is good, false otherwise.
      */
      //virtual bool passSelection(const xAOD::Muon& mu) const override;

      /**
      * @brief The loose selection needed by some background estimates.
      *
      * @param mu
      * @return
      */
      //virtual bool passSelectionLoose(const xAOD::Muon& mu) const override;

      ///Print the cuts to the ostream.
      //virtual void print(std::ostream& os) const override;
      int isPIDTight(const xAOD::Muon& mu) const;
      int isIsoTight(const xAOD::Muon& mu) const;
      int truthTypeCondensed(const xAOD::Muon& mu) const;

    protected:
      ///Lower pT threshold to apply to object selection.
      double m_ptcut;

      ///Proper tool to select muons.
      asg::AnaToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool;
      asg::AnaToolHandle<CP::IIsolationSelectionTool> m_muonIsolationTool;
      asg::AnaToolHandle<CP::IClassificationTool> m_classifyTool;
      //ToolHandle<CP::IMuonSelectionTool> m_muonSelectionTool;
      //ToolHandle<CP::IMuonSelectionTool> m_muonSelectionToolLoose;

      ///Isolation tool, can be nullptr meaning "no isolation requirement"
      //std::unique_ptr<top::IsolationBase> m_isolation;    
       
  };


}

#endif
