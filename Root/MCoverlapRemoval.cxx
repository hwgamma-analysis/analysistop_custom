/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

//using namespace MCTruthPartClassifier;
#include "HowtoExtendAnalysisTop/MCoverlapRemoval.h"
#include "TopEvent/EventTools.h"

namespace top{
  MCoverlapRemoval::MCoverlapRemoval() :
    m_vgammaORtool_1lep("VGammaORTool/vgammaORTool1lep"),
    m_vgammaORtool_2lep("VGammaORTool/vgammaORTool2lep"),
    m_vgammaORtool_3lep("VGammaORTool/vgammaORTool3lep"),
    m_vgammaORtool_4lep("VGammaORTool/vgammaORTool4lep"),
    m_vgammaORtool_1lep2pho("VGammaORTool/vgammaORTool1lep2pho"),
    m_vgammaORtool_2lep2pho("VGammaORTool/vgammaORTool2lep2pho")
  {

    top::check(m_vgammaORtool_1lep.setProperty("photon_pT_cuts",std::vector<float>({25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_1lep.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_1lep.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_1lep.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_1lep.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_1lep.setProperty("n_leptons",1),"Failed to set nLepton" );
    top::check(m_vgammaORtool_1lep.retrieve() , "Failed to retrieve vgammORtool_1lep" );

    top::check(m_vgammaORtool_2lep.setProperty("photon_pT_cuts",std::vector<float>({25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_2lep.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_2lep.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_2lep.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_2lep.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_2lep.setProperty("n_leptons",2),"Failed to set nLepton" );
    top::check(m_vgammaORtool_2lep.retrieve() , "Failed to retrieve vgammORtool_2lep" );

    top::check(m_vgammaORtool_3lep.setProperty("photon_pT_cuts",std::vector<float>({25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_3lep.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_3lep.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_3lep.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_3lep.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_3lep.setProperty("n_leptons",3),"Failed to set nLepton" );
    top::check(m_vgammaORtool_3lep.retrieve() , "Failed to retrieve vgammORtool_2lep" );

    top::check(m_vgammaORtool_4lep.setProperty("photon_pT_cuts",std::vector<float>({25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_4lep.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_4lep.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_4lep.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_4lep.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_4lep.setProperty("n_leptons",4),"Failed to set nLepton" );
    top::check(m_vgammaORtool_4lep.retrieve() , "Failed to retrieve vgammORtool_2lep" );


    top::check(m_vgammaORtool_1lep2pho.setProperty("photon_pT_cuts",std::vector<float>({25e3,25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_1lep2pho.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_1lep2pho.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_1lep2pho.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_1lep2pho.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_1lep2pho.setProperty("n_leptons",1),"Failed to set nLepton" );
    top::check(m_vgammaORtool_1lep2pho.retrieve() , "Failed to retrieve vgammORtool_1lep" ); 

    top::check(m_vgammaORtool_2lep2pho.setProperty("photon_pT_cuts",std::vector<float>({25e3,25e3})),"Failed to set photon pT cut" );
    top::check(m_vgammaORtool_2lep2pho.setProperty("dR_lepton_photon_cut",0.1),"Failed to set dRcut" );
    top::check(m_vgammaORtool_2lep2pho.setProperty("min_considered_photon_pT",15e3),"Failed to set photon min pT" );
    top::check(m_vgammaORtool_2lep2pho.setProperty("lepton_pdgIds",std::vector<int>({11,-11,13,-13})),"Failed to set leptonPID" );
    top::check(m_vgammaORtool_2lep2pho.setProperty("OutputLevel", MSG::ERROR),"Failed to set dRcut" );
    top::check(m_vgammaORtool_2lep2pho.setProperty("n_leptons",2),"Failed to set nLepton" );
    top::check(m_vgammaORtool_2lep2pho.retrieve() , "Failed to retrieve vgammORtool_2lep" );
  }
  
  int MCoverlapRemoval::checkOR(bool isMC,int mcChannelNumber,const xAOD::TruthParticleContainer* truthPhotons,
              const xAOD::TruthParticleContainer* truthElectrons,
              const xAOD::TruthParticleContainer* truthMuons ){
    std::vector<TLorentzVector>* truthPhoton_p4List = new std::vector<TLorentzVector>();
    std::vector<TLorentzVector>* truthLepton_p4List = new std::vector<TLorentzVector>();
    TLorentzVector truthPhoton_p4,truthLepton_p4;
    for (auto truthPhoton : *truthPhotons){

        if(truthPhoton->nParents()){
          auto parent=truthPhoton->parent(0);
          auto parentID=parent->pdgId();

          if( (std::abs(parentID)>1) &&(std::abs(parentID)<9)) continue;
          if( std::abs(parentID)==15) continue;
          if( std::abs(parentID)>100) continue;
        }   
        truthPhoton_p4.SetPtEtaPhiE(truthPhoton->pt(),truthPhoton->eta(),truthPhoton->phi(),truthPhoton->e());
        truthPhoton_p4List->push_back(truthPhoton_p4);

    }

    for (auto truthElectron : *truthElectrons){
        if (truthElectron->pt()/1000.0<25) continue;

        truthLepton_p4.SetPtEtaPhiE(truthElectron->pt(),truthElectron->eta(),truthElectron->phi(),truthElectron->e());
        truthLepton_p4List->push_back(truthLepton_p4);
        
    }

    for (auto truthMuon : *truthMuons){
      if (truthMuon->pt()/1000.0<25) continue;
        truthLepton_p4.SetPtEtaPhiE(truthMuon->pt(),truthMuon->eta(),truthMuon->phi(),truthMuon->e());
        truthLepton_p4List->push_back(truthLepton_p4);
        
    }

    bool passOR=true;
    bool inOR1=true;
    bool inOR2=true;
    bool inOR3=true;
    bool inOR4=true;
    bool inOR1lep2pho=true;
    bool inOR2lep2pho=true;
    // Sample Overlap Removal
    // W+jets - Remove events in OR
    if (isMC && (mcChannelNumber >= 364156) && (mcChannelNumber <= 364197)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (!inOR1 && !inOR1lep2pho);
    }
    if (isMC && (mcChannelNumber >= 700338) && (mcChannelNumber <= 700346)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (!inOR1 && !inOR1lep2pho);
    }
    // QCD W+gamma - Remove events not in OR
    if (isMC && (mcChannelNumber >= 700402) && (mcChannelNumber <= 700404)){
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR1 && !inOR1lep2pho);
    }

    if (isMC && (mcChannelNumber >= 700015) && (mcChannelNumber <= 700017)){
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR1 && !inOR1lep2pho);
    }

    // EWK W+gamma - Remove events not in OR
    if (isMC && (mcChannelNumber >= 363270) && (mcChannelNumber <= 363272)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR1 && !inOR1lep2pho);
    }
    // Wyy
    if (isMC && (mcChannelNumber >= 700199) && (mcChannelNumber <= 700201)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_1lep2pho->inOverlap(inOR1lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR1 && inOR1lep2pho);
      
    }

    // Z+ jets - Remove events in OR
    if (isMC && (mcChannelNumber >= 364100) && (mcChannelNumber <= 364141)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (!inOR2 && !inOR2lep2pho);
    }

    if (isMC && (mcChannelNumber >= 700320) && (mcChannelNumber <=700331  )){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (!inOR2 && !inOR2lep2pho);
    }

    // Z+gamma - Remove events not in OR
    if (isMC && (mcChannelNumber >= 700011) && (mcChannelNumber <= 700013)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR2 && !inOR2lep2pho);
    }

    if (isMC && (mcChannelNumber >= 700398) && (mcChannelNumber <= 700400)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR2 && !inOR2lep2pho);
    }

    // EWK Zy
    if (isMC && (mcChannelNumber >= 363266) && (mcChannelNumber <= 363268)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR2 && !inOR2lep2pho);
    }
    // Zyy
    if (isMC && (mcChannelNumber >= 700195) && (mcChannelNumber <= 700197)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep2pho->inOverlap(inOR2lep2pho,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR2 && inOR2lep2pho);
    }

    // ttbar - Remove events in OR only for nonallhad sample
    if (isMC && (mcChannelNumber == 410470)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (!(inOR1))&&(!inOR2);
    }
    // ttgamma - Remove events not in OR
    if (isMC && (mcChannelNumber == 410389)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = (inOR1);
    }

    //VV:
    if (isMC && (mcChannelNumber == 364250)){
      top::check(m_vgammaORtool_4lep->inOverlap(inOR4,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = !inOR4;
    }
    if (isMC && (mcChannelNumber == 364253)){
      top::check(m_vgammaORtool_3lep->inOverlap(inOR3,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = !inOR3;
    }
    if (isMC && (mcChannelNumber == 364252)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = !inOR2;
    }
    if (isMC && (mcChannelNumber == 364251)){
      top::check(m_vgammaORtool_1lep->inOverlap(inOR1,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = !inOR1;
    }
    //VVy
    if (isMC && (mcChannelNumber == 366162)){
      top::check(m_vgammaORtool_4lep->inOverlap(inOR4,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = inOR4;
    }
    if (isMC && (mcChannelNumber == 366160)){
      top::check(m_vgammaORtool_3lep->inOverlap(inOR3,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = inOR3;
    }
    if (isMC && (mcChannelNumber == 366161)){
      top::check(m_vgammaORtool_2lep->inOverlap(inOR2,truthLepton_p4List,truthPhoton_p4List), "Failed to check OR");
      passOR = inOR2;
    }

    //free memory
    delete truthPhoton_p4List;
    delete truthLepton_p4List;

    return int(passOR);
    }

}

