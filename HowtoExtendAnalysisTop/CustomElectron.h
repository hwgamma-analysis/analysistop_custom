/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMELECTRON_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMELECTRON_H

//#include "TopObjectSelectionTools/electronSelectionBase.h"
#include "TopObjectSelectionTools/IsolationTools.h"

#include "AsgTools/ToolHandle.h"
#include <AsgTools/AnaToolHandle.h>
// egamma tools
#include <EgammaAnalysisInterfaces/IAsgElectronLikelihoodTool.h>
#include <EgammaAnalysisInterfaces/IEgammaCalibrationAndSmearingTool.h>
// isolation tool
#include <IsolationSelection/IIsolationSelectionTool.h>
#include "TruthClassification/TruthClassificationTool.h"


namespace top{
    class CustomElectron{
  
    public:
      CustomElectron(TString Iso);
      
      virtual ~CustomElectron(){}
      
      int isPIDTight(const xAOD::Electron* e) const;
      int isIsoTight(const xAOD::Electron& e) const;
      int truthTypeCondensed(const xAOD::Electron& e) const;

    protected:
      ///Lower pT threshold to apply to object selection.
      double m_ptcut;

       // CP egamma tools
      asg::AnaToolHandle<IAsgElectronLikelihoodTool> m_electronSelectionTool;
      //asg::AnaToolHandle<CP::IEgammaCalibrationAndSmearingTool> m_electronCalibrationAndSmearingTool;
      // CP isolation tool
      asg::AnaToolHandle<CP::IIsolationSelectionTool> m_electronIsolationTool;    
      asg::AnaToolHandle<CP::IClassificationTool> m_classifyTool;

  };


}

#endif
