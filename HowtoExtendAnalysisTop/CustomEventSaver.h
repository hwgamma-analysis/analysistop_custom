/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H
#define HOWTOEXTENDANALYSISTOP_CUSTOMEVENTSAVER_H

#include "TopAnalysis/EventSaverFlatNtuple.h"
#include "HowtoExtendAnalysisTop/CustomJet.h"
#include "HowtoExtendAnalysisTop/CustomMuon.h"
#include "HowtoExtendAnalysisTop/CustomElectron.h"
#include "HowtoExtendAnalysisTop/CustomPhoton.h"
#include "HowtoExtendAnalysisTop/NeutrinoBuilder.h"
#include <xAODTruth/TruthParticleContainer.h>
#include <HowtoExtendAnalysisTop/MCoverlapRemoval.h>
#include "TopConfiguration/TopConfig.h"
#include <xAODJet/JetContainer.h>
#include "TopEvent/EventTools.h"

/**
 * This class shows you how to extend the flat ntuple to include your own variables
 * 
 * It inherits from top::EventSaverFlatNtuple, which will be doing all the hard work 
 * 
 */

namespace top{
  class CustomEventSaver : public top::EventSaverFlatNtuple {
    public:
      ///-- Default constrcutor with no arguments - needed for ROOT --///
      CustomEventSaver();
      ///-- Destructor does nothing --///
      virtual ~CustomEventSaver(){}
      
      ///-- initialize function for top::EventSaverFlatNtuple --///
      ///-- We will be setting up out custom variables here --///
      virtual void initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches) override;
      
      ///-- Keep the asg::AsgTool happy --///
      virtual StatusCode initialize() override {return StatusCode::SUCCESS;}      
      
      ///-- saveEvent function for top::EventSaverFlatNtuple --///
      ///-- We will be setting our custom variables on a per-event basis --///
      virtual void initEvent();
      virtual void saveEvent(const top::Event& event) override;
      virtual double quadAdd(double x, double y);
      

    private:
      ///-- Some additional custom variables for the output --///
      //top::CustomJet jetselection;
      std::shared_ptr<top::TopConfig> m_topconfig;

      top::CustomElectron electronSelection;
      top::CustomMuon muonSelection;
      top::CustomPhoton photonSelection;
      top::MCoverlapRemoval MCoverlapRemoval;
      top::NeutrinoBuilder NeutrinoBuilder;
      //4mtm:
      xAOD::IParticle::FourMom_t leaLep_p4;
      xAOD::IParticle::FourMom_t subLep_p4;
      xAOD::IParticle::FourMom_t closPho_p4;
      xAOD::IParticle::FourMom_t lepPho_p4;
      
      xAOD::IParticle::FourMom_t particle_p4;
      
      xAOD::IParticle::FourMom_t truthLeaLep_p4;
      xAOD::IParticle::FourMom_t truthClosPho_p4;
      xAOD::IParticle::FourMom_t truthClosNu_p4;
      xAOD::IParticle::FourMom_t truthLepPho_p4;
      xAOD::IParticle::FourMom_t truthLepNu_p4;
      xAOD::IParticle::FourMom_t truthLepPhoNu_p4;
      
      //lepton
      std::vector<float> m_leptonPx;
      std::vector<float> m_leptonPy;
      std::vector<float> m_leptonPz;
      std::vector<float> m_leptonPt;
      std::vector<float> m_leptonE;
      std::vector<float> m_leptonPhi;
      std::vector<float> m_leptonEta;
      std::vector<int> m_leptonID;
      std::vector<int> m_leptonPIDTight;
      std::vector<int> m_leptonIsoTight;
      std::vector<int> m_leptonTruthTypeCondensed;
      std::vector<int> m_leptonDFAmbiguity;
      std::vector<int> m_leptonAmbiguity;
      std::vector<int> m_leptonECIDS;
      std::vector<float> m_leptonTopoEt40;
      
      //photon
      std::vector<float> m_photonPx;
      std::vector<float> m_photonPy;
      std::vector<float> m_photonPz;
      std::vector<float> m_photonPt;
      std::vector<float> m_photonE;
      std::vector<float> m_photonPhi;
      std::vector<float> m_photonEta;
      std::vector<float> m_photonDR;
      std::vector<int> m_photonPIDTight;
      std::vector<int> m_photonIsoTight;
      std::vector<float> m_topoEt40;
      std::vector<float> m_topoEt40Corrected;
      std::vector<int> m_photonTruthTypeCondensed;
      std::vector<int> m_photonPassLoosePrime2;
      std::vector<int> m_photonPassLoosePrime3;
      std::vector<int> m_photonPassLoosePrime4;
      std::vector<int> m_photonPassLoosePrime4a;
      std::vector<int> m_photonPassLoosePrime5;
      int m_closPhoPosition;
      int m_numPho;
      
      //truth leptons
      std::vector<float> m_truthLeptonPx;
      std::vector<float> m_truthLeptonPy;
      std::vector<float> m_truthLeptonPz;
      std::vector<float> m_truthLeptonPt;
      std::vector<float> m_truthLeptonE;
      std::vector<float> m_truthLeptonPhi;
      std::vector<float> m_truthLeptonEta;
      std::vector<float> m_truthLeptonDR;
      std::vector<int> m_truthLeptonID;
      std::vector<int> m_truthLeptonParent;
      
      //truth photons
      std::vector<float> m_truthPhotonPx;
      std::vector<float> m_truthPhotonPy;
      std::vector<float> m_truthPhotonPz;
      std::vector<float> m_truthPhotonPt;
      std::vector<float> m_truthPhotonE;
      std::vector<float> m_truthPhotonPhi;
      std::vector<float> m_truthPhotonEta;
      std::vector<float> m_truthPhotonDR;
      std::vector<int> m_truthPhotonParent;
    
      //truth neutrinos
      std::vector<float> m_truthNeutrinoPx;
      std::vector<float> m_truthNeutrinoPy;
      std::vector<float> m_truthNeutrinoPz;
      std::vector<float> m_truthNeutrinoPt;
      std::vector<float> m_truthNeutrinoE;
      std::vector<float> m_truthNeutrinoPhi;
      std::vector<float> m_truthNeutrinoEta;
      std::vector<float> m_truthNeutrinoDR;
      std::vector<int> m_truthNeutrinoParent;
      
      //truth jets
      std::vector<float> m_truthJetPx;
      std::vector<float> m_truthJetPy;
      std::vector<float> m_truthJetPz;
      std::vector<float> m_truthJetPt;
      std::vector<float> m_truthJetE;
      std::vector<float> m_truthJetPhi;
      std::vector<float> m_truthJetEta;
      std::vector<float> m_truthJetDR;

      //leading jets
      std::vector<float> m_jetPt;
      std::vector<float> m_jetPhi;
      std::vector<float> m_jetEta;

      //Other variables:
      float m_METx;
      float m_METy;
      float m_MET;
      float m_ptlgMET;
      float m_Ht;
      float m_pq;
      float m_invmlg;
      float m_mtlg;
      float m_mtlmet;
      float m_mtlgmet;
      float m_mtllmet;
      float m_invmtruthlg;
      float m_invmtruthlnu;
      float m_invmtruthlgnu;
      bool m_passVyOR;
      int m_isEchannel;
      float m_xsec;
      float m_kfactor;

      float m_invmll;
      float m_ptllMET;
      float m_invmW_realPart;
      float m_invmW_rotation;
      std::vector<float> m_invmW_fit;
      float m_invmWy_realPart;
      float m_invmWy_rotation;
      std::vector<float> m_invmWy_fit;

      float m_neutrinoPx_realPart;
      float m_neutrinoPy_realPart;
      float m_neutrinoPz_realPart;
      float m_neutrinoPx_rotation;
      float m_neutrinoPy_rotation;
      float m_neutrinoPz_rotation;
      std::vector<float> m_neutrinoPx_fit;
      std::vector<float> m_neutrinoPy_fit;
      std::vector<float> m_neutrinoPz_fit;

      std::vector<float> m_cxList={0.1,1,10,100,1000};
      std::vector<float> m_czList={0.01,0.1,1,10,100};


      const xAOD::TruthParticleContainer *m_truthElectrons;
      const xAOD::TruthParticleContainer *m_truthMuons;
      const xAOD::TruthParticleContainer *m_truthPhotons;
      const xAOD::TruthParticleContainer *m_truthTaus;
      const xAOD::TruthParticleContainer *m_truthNeutrinos;
      const xAOD::JetContainer *m_truthJets;

      ///-- Tell RootCore to build a dictionary (we need this) --///
      ClassDefOverride(top::CustomEventSaver, 0);
  };
}

#endif
