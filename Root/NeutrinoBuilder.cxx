


#include "HowtoExtendAnalysisTop/NeutrinoBuilder.h"
#include "TopEvent/EventTools.h"


namespace top{
  NeutrinoBuilder::NeutrinoBuilder(std::string units) 
  {

    if (units == "MeV")
        m_Units = 1000.;
    else if (units == "GeV")
        m_Units = 1.;
    else
        std::cerr << "WARNING in NeutrinoBuilder: units different from GeV or MeV" << std::endl;
  }
    
    //_________________________________________________________________________________________________
    void delta2_fcn(Int_t & /*npar*/, Double_t * /*grad*/, Double_t &f, Double_t *par, Int_t /*iflag*/)
    {

    Double_t delta2 = 0;
    Double_t alpha = par[0];
    Double_t r = par[1];
    Double_t dphi = par[2];
    Double_t l_pt = par[3];
    Double_t l_m = par[4];
    Double_t n_px = par[5];
    Double_t n_py = par[6];
    r /= sqrt(l_pt * l_pt + l_m * l_m) - l_pt * cos(dphi + alpha);
    TLorentzVector *neut = new TLorentzVector(n_px, n_py, 0., 0.);
    neut->SetE(neut->P());

    TLorentzVector *neut_new = new TLorentzVector(r * neut->P() * cos(neut->Phi() + alpha), r * neut->P() * sin(neut->Phi() + alpha), 0., 0.);
    neut_new->SetE(neut_new->P());

    delta2 = pow((neut_new->Px() - neut->Px()), 2) + pow((neut_new->Py() - neut->Py()), 2);
    r *= sqrt(l_pt * l_pt + l_m * l_m) - l_pt * cos(dphi + alpha);
    delete neut;
    delete neut_new;
    f = delta2;
    }

    //_________________________________________________________________________________________________
    Double_t NeutrinoBuilder::fitAlpha(const TLorentzVector *L, const Double_t met, const Double_t metphi)
    {
    //  std::cout<<"in fitAlpha"<<std::endl;
    // initialize
    double m_mWpdg = 80.4 * m_Units;
    Double_t pxNu = met * cos(metphi);
    Double_t pyNu = met * sin(metphi);
    Double_t ptNu = met;
    //  std::cout<<"after double "<<std::endl;
    TMinuit *fit = new TMinuit(7);
    fit->SetFCN(delta2_fcn);
    //  std::cout<<"after Fit "<<std::endl;
    int ierr = 0;
    double arglist[1] = {-1};
    fit->mnexcm("SET PRIN", arglist, 1, ierr);
    //  std::cout<<"after SET PRIN "<<std::endl;

    //  std::cout<<"after init "<<std::endl;
    // Initialise the parameters
    std::string par_name[7] = {"alpha", "r", "dphi", "l_pt", "l_m", "n_px", "n_py"};
    Double_t par_ival[7] = {0., (m_mWpdg * m_mWpdg - L->M() * L->M()) / (2 * ptNu), metphi - L->Phi(), L->Pt(), L->M(), pxNu, pyNu};
    Double_t par_step[7] = {0.1, 0., 0., 0., 0., 0., 0.};
    Double_t par_min[7] = {-3.15, 0., -3.15, 0., 0., -10000., -10000.};
    Double_t par_max[7] = {3.15, 1., 3.15, 10000., 80., 10000., 10000.};
    //  std::cout<<"after Param "<<std::endl;
    for (Int_t i = 0; i < 7; i++)
    {
        fit->DefineParameter(i, par_name[i].c_str(), par_ival[i], par_step[i], par_min[i], par_max[i]);
        if (i != 0)
        {
        fit->FixParameter(i);
        }
    }
    //    std::cout<<"after i loop "<<std::endl;


    fit->SetPrintLevel(-1);
    fit->Migrad();
    Double_t a, e_a;
    Int_t ret = fit->GetParameter(0, a, e_a);
    
    //  std::cout<<"after Fit"<<std::endl;
    delete fit;
    //  std::cout<<"after deletion"<<std::endl;
    if (ret > 0)
    {
        //    std::cout<<"ret >0 "<<a<<std::endl;
        return a;
    }
    else
    {
        std::cout << "Error in fit of alpha for met correction" << std::endl;
        return 0.;
    }
    }

    // In case of negative discriminant, decrese the MET
    //_________________________________________________________________________________________________
    std::vector<TLorentzVector *> NeutrinoBuilder::candidatesFromWMass_Rotation(const TLorentzVector *L, const TLorentzVector *MET, const bool useSmallestPz)
    {
    return this->candidatesFromWMass_Rotation(L, MET->Pt(), MET->Phi(), useSmallestPz);
    }

    // In case of negative discriminant, decrese the MET
    //_________________________________________________________________________________________________
    std::vector<TLorentzVector *> NeutrinoBuilder::candidatesFromWMass_Rotation(const TLorentzVector *L, const Double_t met, const Double_t metphi, const bool useSmallestPz)
    {
    //  if (m_debug > 0)
    //  std::cout << "entering candidatesFromWMassRotation()" << std::endl;

    // initialize
    Double_t m_mWpdg = 80.4 * m_Units;
    Double_t pxNu = met * cos(metphi);
    Double_t pyNu = met * sin(metphi);
    Double_t pzNu = -1000000;
    Double_t ptNu = met;
    Double_t eNu;
    
    //  std::cout<<"after init "<<std::endl;
    m_isRotated = false;
    m_r = -1.0;

    std::vector<TLorentzVector *> NC;

    Double_t c1 = m_mWpdg * m_mWpdg - L->M() * L->M() + 2 * (L->Px() * pxNu + L->Py() * pyNu);
    Double_t b1 = 2 * L->Pz();

    Double_t A = 4 * pow(L->E(), 2) - b1 * b1;
    Double_t B = -2 * c1 * b1;
    Double_t C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
    Double_t discr = B * B - 4 * A * C;
    Double_t r = 1;

    Double_t sol1, sol2;
    //  std::cout<<"after init2 "<<std::endl;
    if (discr > 0)
    {
        //    std::cout<<"in if "<<std::endl;
        sol1 = (-B + sqrt(discr)) / (2 * A);
        sol2 = (-B - sqrt(discr)) / (2 * A);
    }
    else
    {
        //    std::cout<<"in else "<<std::endl;
        m_isRotated = true;
        Double_t alpha = fitAlpha(L, met, metphi);
        //    std::cout<<"alpha : "<<alpha<<std::endl;
        //    std::cout<<metphi<<" "<<L->Phi()<<std::endl;
        Double_t dphi = metphi - L->Phi();
        //    std::cout<<"dphi : "<<dphi<<std::endl;
        r = (pow(m_mWpdg, 2) - pow(L->M(), 2)) / (2 * ptNu * (sqrt(pow(L->Pt(), 2) + pow(L->M(), 2)) - L->Pt() * cos(dphi + alpha)));
        m_r = r;
        //    std::cout<<"after r "<<std::endl;
        Double_t old_p = ptNu;
        Double_t old_phi = metphi;
        pxNu = r * old_p * cos(old_phi + alpha);
        pyNu = r * old_p * sin(old_phi + alpha);
        ptNu = sqrt(pxNu * pxNu + pyNu * pyNu);

        //    std::cout<<"after sqrt "<<std::endl;
        c1 = m_mWpdg * m_mWpdg - pow(L->M(), 2) + 2 * (L->Px() * pxNu + L->Py() * pyNu);
        B = -2 * c1 * b1;
        C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
        discr = B * B - 4 * A * C;

        //    std::cout<<"after discr "<<std::endl;

        sol1 = -B / (2 * A);
        sol2 = -B / (2 * A);
        //    std::cout<<"ZE end "<<std::endl;

    }
    //  std::cout<<"after if and else "<<std::endl;

    if (useSmallestPz)
    {

        pzNu = (fabs(sol1) > fabs(sol2)) ? sol2 : sol1;

        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu1 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        NC.push_back(nu1);
    }
    else
    {

        pzNu = sol1;
        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu1 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        pzNu = sol2;
        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu2 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        NC.push_back(nu1);
        NC.push_back(nu2);
    }
    //  std::cout<<"after useSmallestPz "<<std::endl;
    
    if (m_debug > 0)
        std::cout << "quitting NeutrinoBuilder::candidatesFromWMassRotation() : " << NC.size() << std::endl;
    return NC;
    }

    // In case of negative discriminant, use the real part
    //_________________________________________________________________________________________________
    std::vector<TLorentzVector *> NeutrinoBuilder::candidatesFromWMass_RealPart(const TLorentzVector *L, const TLorentzVector *MET, const bool useSmallestPz,Double_t mWpdg)
    {
    return this->candidatesFromWMass_RealPart(L, MET->Pt(), MET->Phi(), useSmallestPz,mWpdg);
    }
    // In case of negative discriminant, use the real part
    //_________________________________________________________________________________________________
    std::vector<TLorentzVector *> NeutrinoBuilder::candidatesFromWMass_RealPart(const TLorentzVector *L, Double_t met, Double_t metphi, const bool useSmallestPz, Double_t mWpdg)
    {
    if (m_debug > 0)
        std::cout << "entering candidatesFromWMass_RealPart()" << std::endl;

    // initialize
    Double_t m_mWpdg = mWpdg * m_Units;
    Double_t pxNu = met * cos(metphi);
    Double_t pyNu = met * sin(metphi);
    Double_t pzNu = -1000000;
    Double_t ptNu = met;
    Double_t eNu;

    std::vector<TLorentzVector *> NC;

    Double_t c1 = m_mWpdg * m_mWpdg - L->M() * L->M() + 2 * (L->Px() * pxNu + L->Py() * pyNu);
    Double_t b1 = 2 * L->Pz();

    Double_t A = 4 * pow(L->E(), 2) - b1 * b1;
    Double_t B = -2 * c1 * b1;
    Double_t C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
    Double_t discr = B * B - 4 * A * C;

    Double_t sol1, sol2;
    if (discr > 0)
    {
        sol1 = (-B + sqrt(discr)) / (2 * A);
        sol2 = (-B - sqrt(discr)) / (2 * A);
    }
    //if discr<0
    else
    {
        pzNu = -B / (2 * A);

        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu1 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        NC.push_back(nu1);
        return NC;
    }

    if (useSmallestPz)
    {

        pzNu = (fabs(sol1) > fabs(sol2)) ? sol2 : sol1;

        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu1 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        NC.push_back(nu1);
    }
    else
    {

        pzNu = sol1;
        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu1 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        pzNu = sol2;
        eNu = sqrt(pxNu * pxNu + pyNu * pyNu + pzNu * pzNu);
        TLorentzVector *nu2 = new TLorentzVector(pxNu, pyNu, pzNu, eNu);
        NC.push_back(nu1);
        NC.push_back(nu2);
    }

    if (m_debug > 0)
        std::cout << "quitting NeutrinoBuilder::candidatesFromWMass_RealPart() : " << NC.size() << std::endl;
    return NC;
    }


    //_________________________________________________________________________________________________
    Double_t NeutrinoBuilder::getDiscriminant(const TLorentzVector *L, Double_t met, Double_t metphi)
    {

    // initialize
    Double_t m_mWpdg = 80.4 * m_Units;
    Double_t pxNu = met * cos(metphi);
    Double_t pyNu = met * sin(metphi);
    Double_t ptNu = met;

    Double_t c1 = m_mWpdg * m_mWpdg - L->M() * L->M() + 2 * (L->Px() * pxNu + L->Py() * pyNu);
    Double_t b1 = 2 * L->Pz();

    Double_t A = 4 * pow(L->E(), 2) - b1 * b1;
    Double_t B = -2 * c1 * b1;
    Double_t C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
    Double_t discr = B * B - 4 * A * C;
    return discr;
    }

    Double_t getDiscriminant_double(const TLorentzVector *L, Double_t met, Double_t metphi)
    {

    // initialize
    Double_t m_mWpdg = 80.4 ;
    Double_t pxNu = met * cos(metphi);
    Double_t pyNu = met * sin(metphi);
    Double_t ptNu = met;

    Double_t c1 = m_mWpdg * m_mWpdg - L->M() * L->M() + 2 * (L->Px() * pxNu + L->Py() * pyNu);
    Double_t b1 = 2 * L->Pz();

    Double_t A = 4 * pow(L->E(), 2) - b1 * b1;
    Double_t B = -2 * c1 * b1;
    Double_t C = 4 * pow(L->E(), 2) * ptNu * ptNu - c1 * c1;
    Double_t discr = B * B - 4 * A * C;
    return discr;
    }

    void fitMinizationFunction(Int_t & /*npar*/, Double_t * /*grad*/, Double_t &f, Double_t *par, Int_t /*iflag*/)
    {

    Double_t m_mWpdg = 80.4;
    Double_t pxNu=par[0];
    Double_t pyNu=par[1];
    Double_t pzNu=par[2];
    Double_t ENu=sqrt(pxNu*pxNu+pyNu*pyNu+pzNu*pzNu);

    
    Double_t lepPx=par[3];
    Double_t lepPy=par[4];
    Double_t lepPz=par[5];
    Double_t lepE=par[6];

    Double_t pxNu_estimate=par[7];
    Double_t pyNu_estimate=par[8];
    Double_t pzNu_estimate=par[9];

    Double_t coeff_px=par[10];
    Double_t coeff_py=par[11];
    Double_t coeff_pz=par[12];
    Double_t coeff_mass=par[13];
    
    // Double_t coeff_px=100;
    // Double_t coeff_py=100;
    // Double_t coeff_pz=10;
    // Double_t coeff_mass=1;

    Double_t output=0;

    TLorentzVector *neut = new TLorentzVector(pxNu,pyNu,pzNu,ENu);
    TLorentzVector *lep= new TLorentzVector(lepPx,lepPy,lepPz,lepE);
    //Double_t dMass=(*neut+*lep).M()-m_mWpdg;
    Double_t xSum=lepPx+pxNu;
    Double_t ySum=lepPy+pyNu;
    Double_t zSum=lepPz+pzNu;
    Double_t ESum=lepE+ENu;
    Double_t dMass=(ESum*ESum-xSum*xSum-ySum*ySum-zSum*zSum)-m_mWpdg*m_mWpdg;

    output+=coeff_px*(pxNu-pxNu_estimate)*(pxNu-pxNu_estimate);
    output+=coeff_py*(pyNu-pyNu_estimate)*(pyNu-pyNu_estimate);
    output+=coeff_pz*(pzNu-pzNu_estimate)*(pzNu-pzNu_estimate);
    output+=coeff_mass*(dMass*dMass);



    delete neut;
    delete lep;
    f=output;

    }

    //_________________________________________________________________________________________________
    std::vector<TLorentzVector>  NeutrinoBuilder::candidatesFromWMass_fit(const TLorentzVector *L, Double_t met, Double_t metphi,Double_t c_px,Double_t c_py,Double_t c_pz,Double_t c_mass)
    {

    // initialize
    Double_t m_mWpdg = 80.4 * m_Units;

    Double_t sampledWmass=m_mWpdg;
    TLorentzVector* Nu_estimate=candidatesFromWMass_RealPart(L,met,metphi,true,sampledWmass).at(0);
    //Initializing them to be the same
    Double_t pxNu_preFit = Nu_estimate->Px();
    Double_t pyNu_preFit = Nu_estimate->Py();
    Double_t pzNu_preFit = Nu_estimate->Pz();
    
    Double_t pxNu_estimate = Nu_estimate->Px();
    Double_t pyNu_estimate = Nu_estimate->Py();
    Double_t pzNu_estimate = Nu_estimate->Pz();

    Double_t lepE=L->E();
    Double_t lepPx=L->Px();
    Double_t lepPy=L->Py();
    Double_t lepPz=L->Pz();

    Double_t lepPx_estimate=L->Px();
    Double_t lepPy_estimate=L->Py();
    Double_t lepPz_estimate=L->Pz();

    //  std::cout<<"after double "<<std::endl;
    TMinuit *fit = new TMinuit(14);
    fit->SetFCN(fitMinizationFunction);
    //  std::cout<<"after Fit "<<std::endl;
    int ierr = 0;
    double arglist[1] = {-1};
    fit->mnexcm("SET PRIN", arglist, 1, ierr);
    //  std::cout<<"after SET PRIN "<<std::endl;

    //  std::cout<<"after init "<<std::endl;
    // Initialise the parameters

    Double_t low=50;
    Double_t high=50;

    Double_t leptonLow=3;
    Double_t leptonHigh=3;
    std::string par_name[14] = {"pxNu","pyNu","pzNu","lepPx","lepPy","lepPz","lepE","pxNu_estimate","pyNu_estimate","pzNu_estimate","c_px","c_py","c_pz","c_mass"};
    Double_t par_ival[14] = {pxNu_preFit,pyNu_preFit,pzNu_preFit,lepPx,lepPy,lepPz,lepE,pxNu_estimate,pyNu_estimate,pzNu_estimate,c_px,c_py,c_pz,c_mass};
    Double_t par_step[14] = {0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0., 0., 0., 0.,0.,0.,0.,0.};
    Double_t par_min[14] = {pxNu_preFit-low,pyNu_preFit-low,pzNu_preFit-2*low,lepPx-leptonLow,lepPy-leptonLow,lepPz-leptonLow,lepE,pxNu_estimate,pyNu_estimate,pzNu_estimate,c_px,c_py,c_pz,c_mass};
    Double_t par_max[14] = {pxNu_preFit+high,pyNu_preFit+high,pzNu_preFit+2*high,lepPx+leptonHigh,lepPy+leptonHigh,lepPz+leptonHigh,lepE,pxNu_estimate,pyNu_estimate,pzNu_estimate,c_px,c_py,c_pz,c_mass};
    //  std::cout<<"after Param "<<std::endl;
    for (Int_t i = 0; i < 14; i++)
    {
        fit->DefineParameter(i, par_name[i].c_str(), par_ival[i], par_step[i], par_min[i], par_max[i]);
        if (i >2)
        {
        fit->FixParameter(i);
        }
    }
    //    std::cout<<"after i loop "<<std::endl;


    fit->SetPrintLevel(-1);
    fit->Migrad();
    Double_t pxNu_postFit,pyNu_postFit,pzNu_postFit;
    Double_t e_pxNu_postFit,e_pyNu_postFit,e_pzNu_postFit;
    Int_t ret_x = fit->GetParameter(0, pxNu_postFit, e_pxNu_postFit);
    Int_t ret_y = fit->GetParameter(1, pyNu_postFit, e_pyNu_postFit);
    Int_t ret_z = fit->GetParameter(2, pzNu_postFit, e_pzNu_postFit);

    Double_t NuE_postFit=sqrt(pxNu_postFit*pxNu_postFit+pyNu_postFit*pyNu_postFit+pzNu_postFit*pzNu_postFit);
    TLorentzVector Nu_postFit(pxNu_postFit,pyNu_postFit,pzNu_postFit,NuE_postFit);
    
    Double_t lepPx_postFit,lepPy_postFit,lepPz_postFit;
    Double_t e_lepPx_postFit,e_lepPy_postFit,e_lepPz_postFit;
    Int_t ret_x_lep = fit->GetParameter(3, lepPx_postFit, e_lepPx_postFit);
    Int_t ret_y_lep = fit->GetParameter(4, lepPy_postFit, e_lepPy_postFit);
    Int_t ret_z_lep = fit->GetParameter(5, lepPz_postFit, e_lepPz_postFit);
    Double_t lepMass=L->M();
    Double_t lepE_postFit=sqrt(lepMass*lepMass+lepPx_postFit*lepPx_postFit+lepPy_postFit*lepPy_postFit+lepPz_postFit*lepPz_postFit);
    TLorentzVector lep_postFit(lepPx_postFit,lepPy_postFit,lepPz_postFit,lepE_postFit);

    if (ret_x<0 || ret_y<0 || ret_z<0 || ret_x_lep<0 || ret_y_lep || ret_z_lep<0){
        std::cout<<"ret<0"<<std::endl;
    }

    delete fit;

    std::vector<TLorentzVector> output={};
    output.push_back(Nu_postFit);
    output.push_back(lep_postFit);
    return output;
    }



}
