/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_PTLGMETSELECTOR_H
#define HOWTOEXTENDANALYSISTOP_PTLGMETSELECTOR_H

#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/SignValueSelector.h"

namespace top{
  

  class PtlgMETSelector : public SignValueSelector {
    public:
      explicit PtlgMETSelector(const std::string& params);

      bool apply(const top::Event& event) const override;

      //For humans, something short and catchy
      std::string name() const override;
  };  
  
}

#endif
