/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/PtllMETSelector.h"

namespace top{
    //If the event number divided by two has no remainder then return true.
  PtllMETSelector::PtllMETSelector(const std::string& params) :
    SignValueSelector("PtllMET", params) {
  }
  bool PtllMETSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t close_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closE_dR=10.0;
    for(const xAOD::Electron* e : event.m_electrons){
        float dR = leaLep_p4.DeltaR(e->p4());
        // making sure that this is not the same electron as the leading one by enforcing dR>0.4
        if((dR<closE_dR)&&(dR>0.4)){
            close_p4=e->p4();
        }
    }
    for (const xAOD::Muon* mu : event.m_muons){
        float dR = leaLep_p4.DeltaR(mu->p4());
        if((dR<closE_dR)&&(dR>0.4)){
            close_p4=mu->p4();
        }
    }

    const xAOD::MissingET* met=event.m_met;
    float m_METx=met->mpx();
    float m_METy=met->mpy();

    float m_ptllMETx=m_METx+leaLep_p4.Px()+close_p4.Px();
    float m_ptllMETy=m_METy+leaLep_p4.Py()+close_p4.Py();
    float m_ptllMET=sqrt(m_ptllMETx*m_ptllMETx+m_ptllMETy*m_ptllMETy);
    
    return checkFloat(m_ptllMET, value());
  }

  //For the cutflow and terminal output
  std::string PtllMETSelector::name() const {
      return "PtllMET";
  }
}
