import os
import argparse
import sys
from datetime import datetime

username = "upatel"

parser = argparse.ArgumentParser()
parser.add_argument("--nEvents", "-n", help="Number of events per job")
parser.add_argument("--config", "-c", help="config setting (Vy, EWKVy, or normal)")
parser.add_argument("--fileType", "-f", help="File type (Data or MC)")
parser.add_argument("--period", "-p", help="Campaign period (a, d, or e)")
args=parser.parse_args()
if args.nEvents:
    nEvents_flag = "--nEventsPerJob="+str(args.nEvents)+" "
else:
    nEvents_flag = ""

def file_reader(file_type, campaign,config):

    file_flag = []
    
    if(file_type == "Data"):
        txtfile = open("Data_rel21.txt", "r")
        if(campaign == "a"):
            proc_names = ["DATA15", "DATA16"]
        elif(campaign == "d"):
            proc_names = ["DATA17"]
        else:
            proc_names = ["DATA18"]
    else:
        txtfile = open("MC16_PHYS.txt", "r")
        if(config == "normal"):
            proc_names = [  "Wy/PHYS"+campaign,
                            "Zy/PHYS"+campaign,
                            "tty/PHYS"+campaign,
                            "DiPhoton/PHYS"+campaign,
                            "yjets/PHYS"+campaign,
                            "Vyy/PHYS"+campaign,
                            "VVy/PHYS"+campaign,
                            "ttbar/PHYS"+campaign,
                            "VV/PHYS"+campaign,
                            "signal/PHYS"+campaign
                            ]
        elif(config == "Vy"):
            proc_names = ["Wjets/PHYS"+campaign,"Zjets/PHYS"+campaign]
        elif(config == "EWKVy"):
            proc_names = ["EWKVy/PHYS"+campaign]
        
    lines = txtfile.readlines()
    adding_files = False
    for line in lines:
        if adding_files and line.strip() != "*":
            if(file_type == "MC"):
                file_flag.append(line.strip()[1:-2])
            else:
                file_flag.append(line.strip())
        if line.strip() in proc_names:
            adding_files = True
        if line.strip() == "*":
            adding_files = False
            
    return(file_flag)
        
    



if args.config == "Vy":
    config_file = "/afs/cern.ch/user/u/upatel/ChargedHiggs/AnalysisTop/source/analysistop_custom/share/Vy-config.txt"
elif args.config == "EWKVy":
    config_file = "/afs/cern.ch/user/u/upatel/ChargedHiggs/AnalysisTop/source/analysistop_custom/share/EWK-config.txt"
elif args.config == "normal":
    config_file = "/afs/cern.ch/user/u/upatel/ChargedHiggs/AnalysisTop/source/analysistop_custom/share/custom-saver-test.txt"
else:
    sys.exit("Please specify a proper config file setting")

if args.fileType and args.period and args.config:
    file_flags = file_reader(args.fileType, args.period, args.config)
else:
    sys.exit("Please specify the file type and the campaign period")


prun_comm_str = "prun "
set_flags = "--useAthenaPackages --cmtConfig=x86_64-centos7-gcc8-opt --writeInputToTxt=IN:in.txt --outputs=output_root:output.root --mergeOutput --outTarBall=top-xaod.tar.gz  --memory=4000 --maxFileSize=1000000000 "
exec_flag = "--exec=\"top-xaod "+config_file+" in.txt\" "

unique_id = datetime.today().strftime('%Y-%m-%d-%H-%M')

for f in file_flags:
    list_f = f.split('.')
    inDS_flag = "--inDS="+list_f[0]+":"+f+" "
    out_f = f.replace(list_f[0],"")
    
    outDS_flag = "--outDS=user."+username+out_f+"."+unique_id+" "
    
    full_prun_statement = prun_comm_str+inDS_flag+outDS_flag+set_flags+exec_flag+nEvents_flag
    
    print(full_prun_statement)

    os.system(full_prun_statement)



