

#ifndef HOWTOEXTENDANALYSISTOP_NEUTRINOBUILDER_H
#define HOWTOEXTENDANALYSISTOP_NEUTRINOBUILDER_H

#include <iostream>
#include <string>

// ROOT classes
#include <TLorentzVector.h>
#include <TMinuit.h>
#include <RooRealVar.h>
#include <RooBreitWigner.h>
#include <RooDataSet.h>
//#include <TUUID.h>



namespace top{
    class NeutrinoBuilder{
  
    public:
        NeutrinoBuilder(std::string units);
        virtual ~NeutrinoBuilder(){}
  
  bool m_isRotated;
  double m_r;


        Double_t getDiscriminant(const TLorentzVector*, const Double_t, const Double_t);
        // In case of negative discriminant, use the real part
        
     // In case of negative discriminant, rotate the MET
        std::vector<TLorentzVector*> candidatesFromWMass_Rotation(const TLorentzVector*,  const Double_t, const Double_t, const bool);
        std::vector<TLorentzVector*> candidatesFromWMass_Rotation(const TLorentzVector*,  const TLorentzVector*, const bool);
        std::vector<TLorentzVector*> candidatesFromWMass_RealPart(const TLorentzVector*,  const Double_t, const Double_t, const bool, const Double_t);
        std::vector<TLorentzVector*> candidatesFromWMass_RealPart(const TLorentzVector*,  const TLorentzVector*, const bool, const Double_t);
        std::vector<TLorentzVector>  candidatesFromWMass_fit(const TLorentzVector* , const Double_t, const Double_t,const Double_t,const Double_t,const Double_t,const Double_t);

    protected:
        int m_debug=0.0;
        double m_Units;  
        double fitAlpha(const TLorentzVector*, const Double_t, const Double_t);
  };


}

#endif
