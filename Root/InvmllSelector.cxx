/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/InvmllSelector.h"

namespace top{

  InvmllSelector::InvmllSelector(const std::string& params) :
    SignValueSelector("INVMLL", params) {
  }


    //If the event number divided by two has no remainder then return true.
  bool InvmllSelector::apply(const top::Event& event) const {

    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t lepPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;

    for(const xAOD::Electron* e : event.m_electrons) {
        float dR = leaLep_p4.DeltaR(e->p4());
        if (dR<0.4) continue; // so it is not the same particle
        if(dR<closPho_dR){
            lepPho_p4=leaLep_p4+e->p4();
            closPho_dR=dR;
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        float dR = leaLep_p4.DeltaR(mu->p4());
        if (dR<0.4) continue; // so it is not the same particle
        if(dR<closPho_dR){
            lepPho_p4=leaLep_p4+mu->p4();
            closPho_dR=dR;
        }
    }

    double Invmll_diff=std::abs(lepPho_p4.M()*0.001-91.1876);



    return checkFloat(Invmll_diff, value());
  }

  //For the cutflow and terminal output
  std::string InvmllSelector::name() const {
      return "INVMLL";
  }
}
