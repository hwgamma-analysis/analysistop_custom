/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/PtlgMETSelector.h"

namespace top{

  PtlgMETSelector::PtlgMETSelector(const std::string& params) :
    SignValueSelector("PtlgMET", params) {
  }

    //If the event number divided by two has no remainder then return true.
  bool PtlgMETSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t closPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;
    for(const xAOD::Photon* pho : event.m_photons){
        float dR = leaLep_p4.DeltaR(pho->p4());
        if(dR<closPho_dR){
            closPho_p4=pho->p4();
        }
    }

    const xAOD::MissingET* met=event.m_met;
    float m_METx=met->mpx();
    float m_METy=met->mpy();

    float m_ptlgMETx=m_METx+leaLep_p4.Px()+closPho_p4.Px();
    float m_ptlgMETy=m_METy+leaLep_p4.Py()+closPho_p4.Py();
    float m_ptlgMET=sqrt(m_ptlgMETx*m_ptlgMETx+m_ptlgMETy*m_ptlgMETy);
    
    return checkFloat(m_ptlgMET, value());
  }

  //For the cutflow and terminal output
  std::string PtlgMETSelector::name() const {
      return "PtlgMET";
  }
}
