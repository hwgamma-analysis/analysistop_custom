/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/MtlMETSelector.h"

namespace top{

  MtlMETSelector::MtlMETSelector(const std::string& params) :
    SignValueSelector("MtlMET", params) {
  }

    //If the event number divided by two has no remainder then return true.
  bool MtlMETSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    const xAOD::MissingET* met=event.m_met;
    float m_METx=met->mpx();
    float m_METy=met->mpy();
    float m_MET=met->met();

    float m_mtlmet = sqrt(leaLep_p4.M()*leaLep_p4.M() + 2*(leaLep_p4.Et()*m_MET - leaLep_p4.Px()*m_METx - leaLep_p4.Py()*m_METy));
    
    return checkFloat(m_mtlmet, value());
  }

  //For the cutflow and terminal output
  std::string MtlMETSelector::name() const {
      return "MtlMET";
  }
}
