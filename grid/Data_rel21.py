# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration

import TopExamples.grid
import TopExamples.ami

TopExamples.grid.Add("DataRetry").datasets = [
    'data15_13TeV:data15_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p4356',
    ]

# Period containers
DATA15 = [#'data15_13TeV:data15_13TeV.periodA.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p4356',
        #'data15_13TeV:data15_13TeV.periodC.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p4356',
        'data15_13TeV:data15_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002',
        'data15_13TeV:data15_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002',
        'data15_13TeV:data15_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002',
        'data15_13TeV:data15_13TeV.periodG.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002',
        'data15_13TeV:data15_13TeV.periodH.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002',
        'data15_13TeV:data15_13TeV.periodJ.physics_Main.PhysCont.DAOD_PHYS.grp15_v01_p5002']

DATA16 = ['data16_13TeV:data16_13TeV.periodA.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodB.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodC.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodG.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodI.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodK.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002',
        'data16_13TeV:data16_13TeV.periodL.physics_Main.PhysCont.DAOD_PHYS.grp16_v01_p5002']

DATA17 = ['data17_13TeV:data17_13TeV.periodI.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodC.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodH.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodK.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodB.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002',
        'data17_13TeV:data17_13TeV.periodN.physics_Main.PhysCont.DAOD_PHYS.grp17_v01_p5002']

DATA18 = ['data18_13TeV:data18_13TeV.periodC.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodD.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodI.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodF.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodM.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodO.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodK.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodE.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodB.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodL.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002',
        'data18_13TeV:data18_13TeV.periodQ.physics_Main.PhysCont.DAOD_PHYS.grp18_v01_p5002']


# Period containers - preferred choice
TopExamples.grid.Add('Data15/PHYS').datasets = [i for i in DATA15]
TopExamples.grid.Add('Data16/PHYS').datasets = [i for i in DATA16]
TopExamples.grid.Add('Data17/PHYS').datasets = [i for i in DATA17]
TopExamples.grid.Add('Data18/PHYS').datasets = [i for i in DATA18]
