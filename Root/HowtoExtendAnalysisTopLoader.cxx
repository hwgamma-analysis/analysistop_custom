/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/HowtoExtendAnalysisTopLoader.h"
#include "HowtoExtendAnalysisTop/EvenEventNumberSelector.h"
#include "HowtoExtendAnalysisTop/InvmlgSelector.h"
#include "HowtoExtendAnalysisTop/PtlgMETSelector.h"
#include "HowtoExtendAnalysisTop/PtllMETSelector.h"
#include "HowtoExtendAnalysisTop/Invmlg25Selector.h"
#include "HowtoExtendAnalysisTop/LepPhoPtSelector.h"
#include "HowtoExtendAnalysisTop/MtlMETSelector.h"
#include "HowtoExtendAnalysisTop/MtlgSelector.h"
#include "HowtoExtendAnalysisTop/MtllSelector.h"
#include "HowtoExtendAnalysisTop/ZyCRFakeSelector.h"
#include "HowtoExtendAnalysisTop/InvmllSelector.h"
#include "TopConfiguration/TopConfig.h"

#include <iostream>
#include "TFile.h"

namespace top{
  top::EventSelectorBase* HowtoExtendAnalysisTopLoader::initTool(const std::string& /*name*/, const std::string& line, TFile* /*outputFile*/, std::shared_ptr<top::TopConfig> /*config*/,EL::Worker* /*wk*/)
  {
    //get the first bit of the string and store it in toolname
    std::istringstream iss(line);
    std::string toolname;
    getline(iss, toolname, ' ');

    //any parameters?
    std::string param;
    if (line.size() > toolname.size())
        param = line.substr(toolname.size() + 1);

    if (toolname == "EVEN")
        return new top::EvenEventNumberSelector(); 
    else if (toolname == "INVMLG")
        return new top::InvmlgSelector(param);
    else if (toolname == "INVMLL")
        return new top::InvmllSelector(param);
    else if (toolname == "MTLG")
        return new top::MtlgSelector(param); 
    else if (toolname == "MTLL")
        return new top::MtllSelector(param);    
    else if (toolname == "MTLMET")
        return new top::MtlMETSelector(param);
    else if (toolname == "LEPPHOPT")
        return new top::LepPhoPtSelector(param);   
    else if (toolname == "LEPLEPPT")
        return new top::LepPhoPtSelector(param);   
    else if (toolname == "INVMLG25")
        return new top::Invmlg25Selector();
    else if (toolname == "PTLGMET")
        return new top::PtlgMETSelector(param); 
    else if (toolname == "PTLLMET")
        return new top::PtllMETSelector(param);
    else if (toolname == "ZyCRFakeSelector")
        return new top::ZyCRFakeSelector();
    //else if (toolname.find("OTHER_TOOL") == 0)
    //  return OtherToolThatYouInvented()    
    
    return nullptr;      
  }
}
