/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_INVMLG25SELECTOR_H
#define HOWTOEXTENDANALYSISTOP_INVMLG25SELECTOR_H

#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"

namespace top{
  
  /**
  * Selects events based on the invariant mass between leading lepton and its closest photon
  */
  class Invmlg25Selector : public top::EventSelectorBase {
    public:
      //This function sees every event.  If you return true then the event passes this "cut"
      bool apply(const top::Event& event) const override;

      //For humans, something short and catchy
      std::string name() const override;
  };  
  
}

#endif
