/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_PTLLMETSELECTOR_H
#define HOWTOEXTENDANALYSISTOP_PTLLMETSELECTOR_H

#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/SignValueSelector.h"

namespace top{
  
  /**
  * Selects events based on the invariant mass between leading lepton and its closest photon
  */
  class PtllMETSelector : public SignValueSelector {
    public:
      explicit PtllMETSelector(const std::string& params);

      bool apply(const top::Event& event) const override;

      //For humans, something short and catchy
      std::string name() const override;
  };  
  
}

#endif
