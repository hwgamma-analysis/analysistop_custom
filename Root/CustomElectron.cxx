/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/CustomElectron.h"

#include "TopEvent/EventTools.h"

namespace top{
  CustomElectron::CustomElectron(TString Iso) :
    m_electronSelectionTool("AsgElectronLikelihoodTool"),
    m_electronIsolationTool("CP::IsolationSelectionTool"),
    m_classifyTool("TruthClassificationTool/myClassificationTool")
  {
    //top::check( m_electronSelectionTool.setProperty("WorkingPoint", ),"Failed to set elecron quality" );
    top::check( m_electronSelectionTool.setProperty("ConfigFile","ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf"),"Failed to set elecron quality" );
    top::check( m_electronSelectionTool.setProperty("primaryVertexContainer","PrimaryVertices"),"Failed to set elecron quality" );
    top::check( m_electronSelectionTool.retrieve() , "Failed to retrieve electronSelectionTool" );

    top::check( m_electronIsolationTool.setProperty("ElectronWP", Iso ),"Failed to set electron isolation" );
    top::check( m_electronIsolationTool.retrieve() , "Failed to retrieve electronIsolationTool" );
    //top::check( m_electronSelectionToolLoose.retrieve() , "Failed to retrieve electronSelectionToolLoose" );

    top::check(m_classifyTool.setProperty("separateChargeFlipElectrons", true), "Failed to set charge Flip");
    top::check(m_classifyTool.initialize(), "Failed to retrieve IFF classification tool");
  }
  
  int CustomElectron::isPIDTight(const xAOD::Electron* e) const 
  {
    if (!m_electronSelectionTool->accept(e)) return 0;
    return 1;
  }

  int CustomElectron::isIsoTight(const xAOD::Electron& e) const
  {
    if (!m_electronIsolationTool->accept(e))   return 0;
    return 1;
  }

  int CustomElectron::truthTypeCondensed(const xAOD::Electron& e) const
  {
  
    unsigned int IFFtype(0);
    top::check(m_classifyTool->classify(e, IFFtype), "Failed to classify electron");
    return IFFtype;
  }

}
