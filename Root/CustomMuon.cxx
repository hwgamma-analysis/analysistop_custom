/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/CustomMuon.h"

#include "TopEvent/EventTools.h"

namespace top{
  CustomMuon::CustomMuon(int PIDcut,TString Iso) :
    m_muonSelectionTool("CP::MuonSelectionTool"),
    m_muonIsolationTool("CP::IsolationSelectionTool"),
    m_classifyTool("TruthClassificationTool/myClassificationTool")
  {
    top::check( m_muonSelectionTool.setProperty("MuQuality", PIDcut),"Failed to set muon quality" );
    top::check( m_muonSelectionTool.retrieve() , "Failed to retrieve muonSelectionTool" );
    top::check( m_muonIsolationTool.setProperty("MuonWP", Iso ),"Failed to set muon isolation" );
    top::check( m_muonIsolationTool.retrieve() , "Failed to retrieve muonIsolationTool" );
    //top::check( m_muonSelectionToolLoose.retrieve() , "Failed to retrieve muonSelectionToolLoose" );
 
    top::check(m_classifyTool.setProperty("separateChargeFlipElectrons", true), "Failed to set charge Flip");
    top::check(m_classifyTool.initialize(), "Failed to retrieve IFF classification tool");
  }
  
  int CustomMuon::isPIDTight(const xAOD::Muon& mu) const 
  {
    if (!m_muonSelectionTool->accept(mu)) return 0;
    return 1;
  }

  int CustomMuon::isIsoTight(const xAOD::Muon& mu) const
  {
    if (!m_muonIsolationTool->accept(mu))   return 0;    
    return 1;
  }

  int CustomMuon::truthTypeCondensed(const xAOD::Muon& mu) const
  {
    unsigned int IFFtype(0);
    top::check(m_classifyTool->classify(mu, IFFtype), "Failed to classify muon");
    return IFFtype;
  }

}
