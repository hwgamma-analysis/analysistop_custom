#include "HowtoExtendAnalysisTop/CustomPhoton.h"
#include "TopEvent/EventTools.h"
#include "ElectronPhotonSelectorTools/egammaPIDdefs.h"

namespace top{
  CustomPhoton::CustomPhoton(TString Iso) :
    m_photonIsolationTool("CP::IsolationSelectionTool")
  {
    top::check( m_photonIsolationTool.setProperty("PhotonWP", Iso ),"Failed to set photon isolation" );
    top::check( m_photonIsolationTool.retrieve() , "Failed to retrieve photonIsolationTool" );
  }

  int CustomPhoton::isIsoTight(const xAOD::Photon& pho) const
  {
    if (!m_photonIsolationTool->accept(pho))   return 0;  
    return 1;
  }

  bool CustomPhoton::passLoosePrime(const xAOD::Photon& pho, int cutNumber) const
  {
    SG::AuxElement::Accessor<unsigned int> isEMTight("DFCommonPhotonsIsEMTightIsEMValue");
    const unsigned int LoosePrime5 = egammaPID::PhotonLoose |
                                         0x1 << egammaPID::ClusterStripsDeltaEmax2_Photon | // never failed in isEMTight anymore, but added since it's included in tight
                                         0x1 << egammaPID::ClusterStripsEratio_Photon; // not the normal "Eratio" quoted

  const unsigned int LoosePrime4 = LoosePrime5 | // also require "wstot"
                                         0x1 << egammaPID::ClusterStripsWtot_Photon;

  // this is the new good one for 2017 data
  const unsigned int LoosePrime4a = LoosePrime5 | // also require "Eratio"
                                          0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;

  const unsigned int LoosePrime3 = LoosePrime4 | // "wstot" + "Eratio"
                                         0x1 << egammaPID::ClusterStripsDEmaxs1_Photon;

  const unsigned int LoosePrime2 = LoosePrime3 | // also require "DeltaE" ("w3" and "Fside" are only remaining requirements for tight)
                                         0x1 << egammaPID::ClusterStripsDeltaE_Photon;


    if(cutNumber==2){
      return !(isEMTight(pho) & LoosePrime2);
    }else if(cutNumber==3){
      return !(isEMTight(pho) & LoosePrime3);
    }else if (cutNumber==4){
      return !(isEMTight(pho) & LoosePrime4);
    }else if (cutNumber==41){
      return !(isEMTight(pho) & LoosePrime4a);
    }else if (cutNumber==5){
      return !(isEMTight(pho) & LoosePrime5);
    }

    return false;

    }

  
  int CustomPhoton::truthTypeCondensed(int truType,int truOrigin) const
  {
    // 0=unknown, 1=photon, 2=hadron, 3=electron, 4=muon
    if (truType == 13 || truType == 14){
    return 1;
  }
  if (truType ==  15 && truOrigin == 9){
    return 1;
  }
  if (truType == 15 && truOrigin == 40){
    return 1;
  }
  if (truType == 16 && truOrigin == 38){
    return 1;
  }
  if (truType == 4 && truOrigin == 6){
    return 2;
  }
  if (truType == 4 && truOrigin == 23){
    return 2;    
  }
  if (truType == 4 && truOrigin == 24){
    return 2;
  }
  if (truType == 4 && truOrigin == 31){
    return 2;
  }
  if (truType == 16 && truOrigin == 6){
    return 2;
  }
  if (truType == 16 && truOrigin == 23){
    return 2;    
  }
  if (truType == 16 && truOrigin == 24){
    return 2;
  }
  if (truType == 16 && truOrigin == 31){
    return 2;
  }
  if (truType == 16 && truOrigin == 30){
    return 2;
  }
  if (truType == 17){
    return 2;
  }
  if (truOrigin == 42){
    return 2;
  }
  if (truOrigin == 26){
    return 2;
  }
  if (truOrigin == 29){
    return 2;
  }
  if (truOrigin == 33){
    return 2;
  }
  if (truOrigin == 25){
    return 2;
  }
  if (truOrigin == 27){
    return 2;
  }
  if (truOrigin == 32){
    return 2;
  }
  if (truType == 1){
    return 3;
  }
  if (truType == 2){
    return 3;
  }
  if (truType == 3){
    return 3;
  }
  if (truType == 5){
    return 4;
  }
  if (truType == 6){
    return 4;
  }
  if (truType == 7){
    return 4;
  }

  return 0;

  }
 
}
