/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/MtlgSelector.h"

namespace top{

  MtlgSelector::MtlgSelector(const std::string& params) :
    SignValueSelector("Mtlg", params) {
  }
  bool MtlgSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t lepPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt() ;
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;
    for(const xAOD::Photon* pho : event.m_photons){
        float dR = leaLep_p4.DeltaR(pho->p4());
        if(dR<closPho_dR){
            lepPho_p4=leaLep_p4+pho->p4();
            closPho_dR=dR;
        }
    }

    float m_mtlg = lepPho_p4.Mt();

    return checkFloat(m_mtlg, value());
  }

  //For the cutflow and terminal output
  std::string MtlgSelector::name() const {
      return "MTLG";
  }
}
