
The AnalysisTop framework should be set up according to:
```
https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TopxAODStartGuideR21
```
Then 
```
cd source
git clone ssh://git@gitlab.cern.ch:7999/hwgamma-analysis/analysistop_custom.git
```

There might be local configuration in the custom-saver-test.txt that you wish to change.
For example, the following might need to be changed to your local address.
```
TDPPath /afs/cern.ch/user/z/zhelun/MyAnalysis/source/XSection-MC16-13TeV.data
```
Run it locally as:
```
top-xaod /afs/cern.ch/user/z/zhelun/MyAnalysis/source/analysistop_custom/share/custom-saver-test.txt [PATH_TO_SAMPLES] 2>&1 | tee logFile.txt
```