/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_MCOVERLAPREMOVAL_H
#define HOWTOEXTENDANALYSISTOP_MCOVERLAPREMOVAL_H

#include <AsgTools/AnaToolHandle.h>
// egamma tools
#include "GammaORTools/VGammaORTool.h"

namespace top{
    class MCoverlapRemoval{
  
    public:
      MCoverlapRemoval();
      
      virtual ~MCoverlapRemoval(){}
      
      
      int checkOR(bool isMC,int mcChannelNumber,const xAOD::TruthParticleContainer* truthPhotons,
                                               const xAOD::TruthParticleContainer* truthElectrons,
                                               const xAOD::TruthParticleContainer* truthMuons );


    protected:
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_1lep;
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_2lep;
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_3lep;
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_4lep;
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_1lep2pho;
      asg::AnaToolHandle<IVGammaORTool> m_vgammaORtool_2lep2pho;
      
      

  };


}

#endif

