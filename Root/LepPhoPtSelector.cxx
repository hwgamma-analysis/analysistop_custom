/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/LepPhoPtSelector.h"

namespace top{

  LepPhoPtSelector::LepPhoPtSelector(const std::string& params) :
    SignValueSelector("LepPhoPt", params) {
  }

    //If the event number divided by two has no remainder then return true.
  bool LepPhoPtSelector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t closPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;
    for(const xAOD::Photon* pho : event.m_photons){
        float dR = leaLep_p4.DeltaR(pho->p4());
        if(dR<closPho_dR){
            closPho_p4=pho->p4();
            closPho_dR=dR;
        }
    }

    float m_lepPhoPt = leaLep_p4.Pt()+closPho_p4.Pt();
    
    return checkFloat(m_lepPhoPt, value());
  }

  //For the cutflow and terminal output
  std::string LepPhoPtSelector::name() const {
      return "LepPhoPt";
  }
}
