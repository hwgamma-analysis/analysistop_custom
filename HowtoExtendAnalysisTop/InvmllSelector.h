/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef HOWTOEXTENDANALYSISTOP_INVMLLSELECTOR_H
#define HOWTOEXTENDANALYSISTOP_INVMLLSELECTOR_H

#include "TopEventSelectionTools/EventSelectorBase.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/SignValueSelector.h"

namespace top{
  
  /**
  * Selects events based on the invariant mass between leading lepton and its closest photon
  */
  class InvmllSelector : public SignValueSelector{
    public:
      //This function sees every event.  If you return true then the event passes this "cut"
      explicit InvmllSelector(const std::string& params);
      bool apply(const top::Event& event) const override;

      //For humans, something short and catchy
      std::string name() const override;
  };  
  
}

#endif
