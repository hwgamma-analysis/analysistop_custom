/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/ZyCRFakeSelector.h"

namespace top{
  bool ZyCRFakeSelector::apply(const top::Event& event) const {
    //ZyCRegammaFakesE always require 3 electrons or 2 muon+1electron
    bool passCut=false;
    if(event.m_electrons.size()==3){
      //echannel
        const xAOD::Electron* e1 = event.m_electrons[0];
        const xAOD::Electron* e2 = event.m_electrons[1];
        const xAOD::Electron* e3 = event.m_electrons[2];
        
        bool e1Photon=(e1->pt()>50000)&&(std::abs((e2->p4()+e3->p4()).M()-91.187)>15)&&(e2->charge()*e3->charge()==-1);
        bool e2Photon=(e2->pt()>50000)&&(std::abs((e1->p4()+e3->p4()).M()-91.187)>15)&&(e1->charge()*e3->charge()==-1);
        bool e3Photon=(e3->pt()>50000)&&(std::abs((e1->p4()+e2->p4()).M()-91.187)>15)&&(e1->charge()*e2->charge()==-1);
        passCut=e1Photon||e2Photon||e3Photon;
    }else if((event.m_muons.size()==2)&&(event.m_electrons.size()==1)){
      //muchannel
        const xAOD::Electron* e = event.m_electrons[0];
        const xAOD::Muon* mu1 = event.m_muons[0];
        const xAOD::Muon* mu2 = event.m_muons[1];
        passCut=(e->pt()>50000)&&(std::abs((mu1->p4()+mu2->p4()).M()-91.187)>15)&&(mu1->charge()*mu2->charge()==-1);
    }
    return passCut;
  }

  //For the cutflow and terminal output
  std::string ZyCRFakeSelector::name() const {
      return "ZyCRFake";
  }
}
