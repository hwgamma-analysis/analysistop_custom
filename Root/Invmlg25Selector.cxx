/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#include "HowtoExtendAnalysisTop/Invmlg25Selector.h"

namespace top{
    //If the event number divided by two has no remainder then return true.
  bool Invmlg25Selector::apply(const top::Event& event) const {
    xAOD::IParticle::FourMom_t leaLep_p4 =xAOD::IParticle::FourMom_t();
    xAOD::IParticle::FourMom_t lepPho_p4 =xAOD::IParticle::FourMom_t();
    float tmpPt=0.0;
    for(const xAOD::Electron* e : event.m_electrons) {
        if(e->pt()>tmpPt){
            tmpPt=e->pt();
            leaLep_p4=e->p4();
        }
    }

    for(const xAOD::Muon* mu : event.m_muons) {
        if(mu->pt()>tmpPt){
            tmpPt=mu->pt();
            leaLep_p4=mu->p4();
        }
    }

    double closPho_dR=10.0;
    for(const xAOD::Photon* pho : event.m_photons){
        float dR = leaLep_p4.DeltaR(pho->p4());
        if(dR<closPho_dR){
            lepPho_p4=leaLep_p4+pho->p4();
            closPho_dR=dR;
        }
    }
    bool passInvmlg=std::abs(lepPho_p4.M()*0.001-91.1876)>25;
    
    return passInvmlg;
  }

  //For the cutflow and terminal output
  std::string Invmlg25Selector::name() const {
      return "INVMLG25";
  }
}
