#include "HowtoExtendAnalysisTop/CustomEventSaver.h"
#include "TopEvent/Event.h"
#include "TopEventSelectionTools/TreeManager.h"
#include <xAODTruth/TruthParticleContainer.h>
#include <TLorentzVector.h>
// ATLAS
#include <PathResolver/PathResolver.h>
#include <TopDataPreparation/SampleXsectionSvc.h>


//#include "HowtoExtendAnalysisTop/CustomJet.h"

namespace top{
  //-- Constructor --//
  CustomEventSaver::CustomEventSaver() :
    electronSelection("PflowTight_FixedRad")//Iso
    ,muonSelection(0,"PflowTight_FixedRad")//PIDTight,Iso
    ,photonSelection("FixedCutTight")
    ,MCoverlapRemoval()
    ,NeutrinoBuilder("GeV")
    ,leaLep_p4(xAOD::IParticle::FourMom_t())
    ,subLep_p4(xAOD::IParticle::FourMom_t())
    ,closPho_p4(xAOD::IParticle::FourMom_t())
    ,lepPho_p4(xAOD::IParticle::FourMom_t())
    ,particle_p4(xAOD::IParticle::FourMom_t())
    ,truthLeaLep_p4(xAOD::IParticle::FourMom_t())
    ,truthClosPho_p4(xAOD::IParticle::FourMom_t())
    ,truthClosNu_p4(xAOD::IParticle::FourMom_t())
    ,truthLepPho_p4(xAOD::IParticle::FourMom_t())
    ,truthLepNu_p4(xAOD::IParticle::FourMom_t())
    ,truthLepPhoNu_p4(xAOD::IParticle::FourMom_t())
    //leading lepton
    ,m_leptonPx(std::vector<float>())
    ,m_leptonPy(std::vector<float>())
    ,m_leptonPz(std::vector<float>())
    ,m_leptonPt(std::vector<float>())
    ,m_leptonE(std::vector<float>())
    ,m_leptonPhi(std::vector<float>())
    ,m_leptonEta(std::vector<float>())
    ,m_leptonID(std::vector<int>())
    ,m_leptonPIDTight(std::vector<int>())
    ,m_leptonIsoTight(std::vector<int>())
    ,m_leptonTruthTypeCondensed(std::vector<int>())
    ,m_leptonDFAmbiguity(std::vector<int>())
    ,m_leptonAmbiguity(std::vector<int>())
    ,m_leptonECIDS(std::vector<int>())
    ,m_leptonTopoEt40(std::vector<float>())
    //photon
    ,m_photonPx(std::vector<float>())
    ,m_photonPy(std::vector<float>())
    ,m_photonPz(std::vector<float>())
    ,m_photonPt(std::vector<float>())
    ,m_photonE(std::vector<float>())
    ,m_photonPhi(std::vector<float>())
    ,m_photonEta(std::vector<float>())
    ,m_photonDR(std::vector<float>())
    ,m_photonPIDTight(std::vector<int>())
    ,m_photonIsoTight(std::vector<int>())
    ,m_topoEt40(std::vector<float>())
    ,m_topoEt40Corrected(std::vector<float>())
    ,m_photonTruthTypeCondensed(std::vector<int>())
    ,m_photonPassLoosePrime2(std::vector<int>())
    ,m_photonPassLoosePrime3(std::vector<int>())
    ,m_photonPassLoosePrime4(std::vector<int>())
    ,m_photonPassLoosePrime4a(std::vector<int>())
    ,m_photonPassLoosePrime5(std::vector<int>())
    ,m_closPhoPosition(-1)
    ,m_numPho(0)
    //truthlepton
    ,m_truthLeptonPx(std::vector<float>())
    ,m_truthLeptonPy(std::vector<float>())
    ,m_truthLeptonPz(std::vector<float>())
    ,m_truthLeptonPt(std::vector<float>())
    ,m_truthLeptonE(std::vector<float>())
    ,m_truthLeptonPhi(std::vector<float>())
    ,m_truthLeptonEta(std::vector<float>())
    ,m_truthLeptonDR(std::vector<float>())
    ,m_truthLeptonID(std::vector<int>())
    ,m_truthLeptonParent(std::vector<int>())
    //truthphoton
    ,m_truthPhotonPx(std::vector<float>())
    ,m_truthPhotonPy(std::vector<float>())
    ,m_truthPhotonPz(std::vector<float>())
    ,m_truthPhotonPt(std::vector<float>())
    ,m_truthPhotonE(std::vector<float>())
    ,m_truthPhotonPhi(std::vector<float>())
    ,m_truthPhotonEta(std::vector<float>())
    ,m_truthPhotonDR(std::vector<float>())
    ,m_truthPhotonParent(std::vector<int>())
    //truthneutrino
    ,m_truthNeutrinoPx(std::vector<float>())
    ,m_truthNeutrinoPy(std::vector<float>())
    ,m_truthNeutrinoPz(std::vector<float>())
    ,m_truthNeutrinoPt(std::vector<float>())
    ,m_truthNeutrinoE(std::vector<float>())
    ,m_truthNeutrinoPhi(std::vector<float>())
    ,m_truthNeutrinoEta(std::vector<float>())
    ,m_truthNeutrinoDR(std::vector<float>())
    ,m_truthNeutrinoParent(std::vector<int>())
    //truthjet
    ,m_truthJetPx(std::vector<float>())
    ,m_truthJetPy(std::vector<float>())
    ,m_truthJetPz(std::vector<float>())
    ,m_truthJetPt(std::vector<float>())
    ,m_truthJetE(std::vector<float>())
    ,m_truthJetPhi(std::vector<float>())
    ,m_truthJetEta(std::vector<float>())
    ,m_truthJetDR(std::vector<float>())
    //leadingJet
    ,m_jetPt(std::vector<float>())
    ,m_jetPhi(std::vector<float>())
    ,m_jetEta(std::vector<float>())
    //other variables
    ,m_METx(0.0)
    ,m_METy(0.0)
    ,m_MET(0.0)
    ,m_ptlgMET(0.0)
    ,m_Ht(0.0)
    ,m_pq(0.0)
    ,m_invmlg(0.0)
    ,m_mtlg(0.0)
    ,m_mtlmet(0.0)
    ,m_mtlgmet(0.0)
    ,m_mtllmet(0.0)
    ,m_invmtruthlg(0.0)
    ,m_invmtruthlnu(0.0)
    ,m_invmtruthlgnu(0.0)
    ,m_passVyOR(1)
    ,m_isEchannel(1)
    ,m_xsec(0)
    ,m_kfactor(0)
    ,m_invmll(0.0)
    ,m_ptllMET(0.0)
    ,m_invmW_realPart(0.0)
    ,m_invmW_rotation(0.0)
    ,m_invmW_fit(std::vector<float>())
    ,m_invmWy_realPart(0.0)
    ,m_invmWy_rotation(0.0)
    ,m_invmWy_fit(std::vector<float>())
    ,m_neutrinoPx_realPart(0.0)
    ,m_neutrinoPy_realPart(0.0)
    ,m_neutrinoPz_realPart(0.0)
    ,m_neutrinoPx_rotation(0.0)
    ,m_neutrinoPy_rotation(0.0)
    ,m_neutrinoPz_rotation(0.0)
    ,m_neutrinoPx_fit(std::vector<float>())
    ,m_neutrinoPy_fit(std::vector<float>())
    ,m_neutrinoPz_fit(std::vector<float>())
  {
  }
  
  ///-- initialize - done once at the start of a job before the loop over events --///
  void CustomEventSaver::initialize(std::shared_ptr<top::TopConfig> config, TFile* file, const std::vector<std::string>& extraBranches)
  {
    ///-- Let the base class do all the hard work --///
    ///-- It will setup TTrees for each systematic with a standard set of variables --///
    top::EventSaverFlatNtuple::initialize(config, file, extraBranches);
    m_topconfig=config;
    ///-- Loop over the systematic TTrees and add the custom variables --///
    for (auto systematicTree : treeManagers()) {
      //lepton
      systematicTree->makeOutputVariable(m_leptonPx,"leptonPx");
      systematicTree->makeOutputVariable(m_leptonPy,"leptonPy");
      systematicTree->makeOutputVariable(m_leptonPz,"leptonPz");
      systematicTree->makeOutputVariable(m_leptonPt,"leptonPt");
      systematicTree->makeOutputVariable(m_leptonE,"leptonE");
      systematicTree->makeOutputVariable(m_leptonPhi,"leptonPhi");
      systematicTree->makeOutputVariable(m_leptonEta,"leptonEta");
      systematicTree->makeOutputVariable(m_leptonID,"leptonID");
      systematicTree->makeOutputVariable(m_leptonPIDTight,"leptonPIDTight");
      systematicTree->makeOutputVariable(m_leptonIsoTight,"leptonIsoTight");
      systematicTree->makeOutputVariable(m_leptonTruthTypeCondensed,"leptonTruthTypeCondensed");
      systematicTree->makeOutputVariable(m_leptonDFAmbiguity,"leptonDFAmbiguity");
      systematicTree->makeOutputVariable(m_leptonAmbiguity,"leptonAmbiguity");
      systematicTree->makeOutputVariable(m_leptonECIDS,"leptonECIDS");
      systematicTree->makeOutputVariable(m_leptonTopoEt40,"leptonTopoEt40");
        
      //Photon
      systematicTree->makeOutputVariable(m_photonPx,"photonPx");
      systematicTree->makeOutputVariable(m_photonPy,"photonPy");
      systematicTree->makeOutputVariable(m_photonPz,"photonPz");
      systematicTree->makeOutputVariable(m_photonPt,"photonPt");
      systematicTree->makeOutputVariable(m_photonE,"photonE");
      systematicTree->makeOutputVariable(m_photonPhi,"photonPhi");
      systematicTree->makeOutputVariable(m_photonEta,"photonEta");
      systematicTree->makeOutputVariable(m_photonDR,"photonDR");
      systematicTree->makeOutputVariable(m_photonPIDTight,"photonPIDTight");
      systematicTree->makeOutputVariable(m_photonIsoTight,"photonIsoTight");
      systematicTree->makeOutputVariable(m_topoEt40,"topoEt40");
      systematicTree->makeOutputVariable(m_topoEt40Corrected,"topoEt40Corrected");
      systematicTree->makeOutputVariable(m_photonTruthTypeCondensed,"photonTruthTypeCondensed");
      systematicTree->makeOutputVariable(m_photonPassLoosePrime2,"photonPassLoosePrime2");
      systematicTree->makeOutputVariable(m_photonPassLoosePrime3,"photonPassLoosePrime3");
      systematicTree->makeOutputVariable(m_photonPassLoosePrime4,"photonPassLoosePrime4");
      systematicTree->makeOutputVariable(m_photonPassLoosePrime4a,"photonPassLoosePrime4a");
      systematicTree->makeOutputVariable(m_photonPassLoosePrime5,"photonPassLoosePrime5");
      systematicTree->makeOutputVariable(m_closPhoPosition,"closPhoPosition");
      systematicTree->makeOutputVariable(m_numPho, "numPho");
      //TruthLepton
      systematicTree->makeOutputVariable(m_truthLeptonPx,"truthLeptonPx");
      systematicTree->makeOutputVariable(m_truthLeptonPy,"truthLeptonPy");
      systematicTree->makeOutputVariable(m_truthLeptonPz,"truthLeptonPz");
      systematicTree->makeOutputVariable(m_truthLeptonPt,"truthLeptonPt");
      systematicTree->makeOutputVariable(m_truthLeptonE,"truthLeptonE");
      systematicTree->makeOutputVariable(m_truthLeptonPhi,"truthLeptonPhi");
      systematicTree->makeOutputVariable(m_truthLeptonEta,"truthLeptonEta");
      systematicTree->makeOutputVariable(m_truthLeptonDR,"truthLeptonDR");
      systematicTree->makeOutputVariable(m_truthLeptonID,"truthLeptonID");
      systematicTree->makeOutputVariable(m_truthLeptonParent,"truthLeptonParent");
      //TruthPhoton
      systematicTree->makeOutputVariable(m_truthPhotonPx,"truthPhotonPx");
      systematicTree->makeOutputVariable(m_truthPhotonPy,"truthPhotonPy");
      systematicTree->makeOutputVariable(m_truthPhotonPz,"truthPhotonPz");
      systematicTree->makeOutputVariable(m_truthPhotonPt,"truthPhotonPt");
      systematicTree->makeOutputVariable(m_truthPhotonE,"truthPhotonE");
      systematicTree->makeOutputVariable(m_truthPhotonPhi,"truthPhotonPhi");
      systematicTree->makeOutputVariable(m_truthPhotonEta,"truthPhotonEta");
      systematicTree->makeOutputVariable(m_truthPhotonDR,"truthPhotonDR");
      systematicTree->makeOutputVariable(m_truthPhotonParent,"truthPhotonParent");
      //TruthNeutrino
      systematicTree->makeOutputVariable(m_truthNeutrinoPx,"truthNeutrinoPx");
      systematicTree->makeOutputVariable(m_truthNeutrinoPy,"truthNeutrinoPy");
      systematicTree->makeOutputVariable(m_truthNeutrinoPz,"truthNeutrinoPz");
      systematicTree->makeOutputVariable(m_truthNeutrinoPt,"truthNeutrinoPt");
      systematicTree->makeOutputVariable(m_truthNeutrinoE,"truthNutrinoE");
      systematicTree->makeOutputVariable(m_truthNeutrinoPhi,"truthNeutrinoPhi");
      systematicTree->makeOutputVariable(m_truthNeutrinoEta,"truthNeutrinoEta");
      systematicTree->makeOutputVariable(m_truthNeutrinoDR, "truthNeutrinoDR");
      systematicTree->makeOutputVariable(m_truthNeutrinoParent,"truthNeutrinoParent");
      //TruthJet
      systematicTree->makeOutputVariable(m_truthJetPx,"truthJetPx");
      systematicTree->makeOutputVariable(m_truthJetPy,"truthJetPy");
      systematicTree->makeOutputVariable(m_truthJetPz,"truthJetPz");
      systematicTree->makeOutputVariable(m_truthJetPt,"truthJetPt");
      systematicTree->makeOutputVariable(m_truthJetE,"truthJetE");
      systematicTree->makeOutputVariable(m_truthJetPhi,"truthJetPhi");
      systematicTree->makeOutputVariable(m_truthJetEta,"truthJetEta");
      systematicTree->makeOutputVariable(m_truthJetDR,"truthJetDR");
      //jets
      systematicTree->makeOutputVariable(m_jetPt,"jetPt");
      systematicTree->makeOutputVariable(m_jetPhi,"jetPhi");
      systematicTree->makeOutputVariable(m_jetEta,"jetEta");
      //others
      systematicTree->makeOutputVariable(m_METx,"METx");
      systematicTree->makeOutputVariable(m_METy,"METy");
      systematicTree->makeOutputVariable(m_MET,"MET");
      systematicTree->makeOutputVariable(m_ptlgMET,"ptlgMET");
      systematicTree->makeOutputVariable(m_Ht,"Ht");
      systematicTree->makeOutputVariable(m_pq,"pq");
      systematicTree->makeOutputVariable(m_invmlg,"invmlg");
      systematicTree->makeOutputVariable(m_mtlg,"mtlg");
      systematicTree->makeOutputVariable(m_mtlmet,"mtlmet");
      systematicTree->makeOutputVariable(m_mtlgmet,"mtlgmet");
      systematicTree->makeOutputVariable(m_mtllmet,"mtllmet");
      systematicTree->makeOutputVariable(m_invmtruthlg, "invmtruthlg");
      systematicTree->makeOutputVariable(m_invmtruthlnu, "invmtruthlnu");
      systematicTree->makeOutputVariable(m_invmtruthlgnu, "invmtruthlgnu");
      systematicTree->makeOutputVariable(m_passVyOR,"passVyOR");
      systematicTree->makeOutputVariable(m_isEchannel,"isEchannel");
      systematicTree->makeOutputVariable(m_xsec,"xsec");
      systematicTree->makeOutputVariable(m_kfactor,"kfactor");
      systematicTree->makeOutputVariable(m_ptllMET,"ptllMET");
      systematicTree->makeOutputVariable(m_invmll,"invmll");
      systematicTree->makeOutputVariable(m_invmW_realPart,"invmW_realPart");
      systematicTree->makeOutputVariable(m_invmWy_realPart,"invmWy_realPart");
      systematicTree->makeOutputVariable(m_invmW_rotation,"invmW_rotation");
      systematicTree->makeOutputVariable(m_invmWy_rotation,"invmWy_rotation");
      systematicTree->makeOutputVariable(m_invmW_fit,"invmW_fit");
      systematicTree->makeOutputVariable(m_invmWy_fit,"invmWy_fit");

      systematicTree->makeOutputVariable(m_neutrinoPx_realPart,"neutrinoPx_realPart");
      systematicTree->makeOutputVariable(m_neutrinoPy_realPart,"neutrinoPy_realPart");
      systematicTree->makeOutputVariable(m_neutrinoPz_realPart,"neutrinoPz_realPart");
      systematicTree->makeOutputVariable(m_neutrinoPx_rotation,"neutrinoPx_rotation");
      systematicTree->makeOutputVariable(m_neutrinoPy_rotation,"neutrinoPy_rotation");
      systematicTree->makeOutputVariable(m_neutrinoPz_rotation,"neutrinoPz_rotation");
      systematicTree->makeOutputVariable(m_neutrinoPx_fit,"neutrinoPx_fit");
      systematicTree->makeOutputVariable(m_neutrinoPy_fit,"neutrinoPy_fit");
      systematicTree->makeOutputVariable(m_neutrinoPz_fit,"neutrinoPz_fit");
    }
  }

  void CustomEventSaver::initEvent()
  {
    leaLep_p4=xAOD::IParticle::FourMom_t();
    subLep_p4=xAOD::IParticle::FourMom_t();
    closPho_p4=xAOD::IParticle::FourMom_t();
    lepPho_p4=xAOD::IParticle::FourMom_t();
    particle_p4=xAOD::IParticle::FourMom_t();
    truthLeaLep_p4=xAOD::IParticle::FourMom_t();
    truthClosPho_p4=xAOD::IParticle::FourMom_t();
    truthClosNu_p4=xAOD::IParticle::FourMom_t();
    truthLepPho_p4=xAOD::IParticle::FourMom_t();
    truthLepNu_p4=xAOD::IParticle::FourMom_t();
    truthLepPhoNu_p4=xAOD::IParticle::FourMom_t();
    // lepton
    m_leptonPx.clear();
    m_leptonPy.clear();
    m_leptonPz.clear();
    m_leptonPt.clear();
    m_leptonE.clear();
    m_leptonPhi.clear();
    m_leptonEta.clear();
    m_leptonID.clear();
    m_leptonPIDTight.clear();
    m_leptonIsoTight.clear();
    m_leptonTruthTypeCondensed.clear();
    m_leptonDFAmbiguity.clear();
    m_leptonAmbiguity.clear();
    m_leptonECIDS.clear();
    m_leptonTopoEt40.clear();

    // photon
    m_photonPx.clear();
    m_photonPy.clear();
    m_photonPz.clear();
    m_photonPt.clear();
    m_photonE.clear();
    m_photonPhi.clear();
    m_photonEta.clear();
    m_photonDR.clear();
    m_photonPIDTight.clear();
    m_photonIsoTight.clear();
    m_photonTruthTypeCondensed.clear();
    m_photonPassLoosePrime2.clear();
    m_photonPassLoosePrime3.clear();
    m_photonPassLoosePrime4.clear();
    m_photonPassLoosePrime4a.clear();
    m_photonPassLoosePrime5.clear();
    m_topoEt40.clear();
    m_topoEt40Corrected.clear();
    m_closPhoPosition=-1;
    m_numPho=0;
    
    // truthlepton
    m_truthLeptonPx.clear();
    m_truthLeptonPy.clear();
    m_truthLeptonPz.clear();
    m_truthLeptonPt.clear();
    m_truthLeptonE.clear();
    m_truthLeptonEta.clear();
    m_truthLeptonPhi.clear();
    m_truthLeptonDR.clear();
    m_truthLeptonID.clear();
    m_truthLeptonParent.clear();
      
    // truthphoton
    m_truthPhotonPx.clear();
    m_truthPhotonPy.clear();
    m_truthPhotonPz.clear();
    m_truthPhotonPt.clear();
    m_truthPhotonE.clear();
    m_truthPhotonEta.clear();
    m_truthPhotonPhi.clear();
    m_truthPhotonDR.clear();
    m_truthPhotonParent.clear();
      
    // truthneutrino
    m_truthNeutrinoPx.clear();
    m_truthNeutrinoPy.clear();
    m_truthNeutrinoPz.clear();
    m_truthNeutrinoPt.clear();
    m_truthNeutrinoE.clear();
    m_truthNeutrinoPhi.clear();
    m_truthNeutrinoEta.clear();
    m_truthNeutrinoDR.clear();
    m_truthNeutrinoParent.clear();
      
    // truthjet
    m_truthJetPx.clear();
    m_truthJetPy.clear();
    m_truthJetPz.clear();
    m_truthJetPt.clear();
    m_truthJetE.clear();
    m_truthJetEta.clear();
    m_truthJetPhi.clear();
    m_truthJetDR.clear();
      
    //jets
    m_jetPt.clear();
    m_jetPhi.clear();
    m_jetEta.clear();
      
    //other variables
    m_METx=0.0;
    m_METy=0.0;
    m_MET=0.0;
    m_ptlgMET=0.0;
    m_Ht=0.0;
    m_pq=0.0;
    m_invmlg=0.0;
    m_mtlg=0.0;
    m_mtlmet=0.0;
    m_mtlgmet=0.0;
    m_mtllmet=0.0;
    m_invmtruthlg = 0.0;
    m_invmtruthlnu = 0.0;
    m_invmtruthlgnu = 0.0;
    m_passVyOR=true;
    m_isEchannel=1;
    m_xsec=0.0;
    m_kfactor=0.0;

    m_invmll=0.0;
    m_ptllMET=0.0;
    m_invmW_realPart=0.0;
    m_invmW_rotation=0.0;
    m_invmW_fit.clear();
    m_invmWy_realPart=0.0;
    m_invmWy_rotation=0.0;
    m_invmWy_fit.clear();

    m_truthElectrons=0;
    m_truthMuons=0;
    m_truthPhotons=0;
    m_truthTaus=0;
    m_truthJets=0;

    m_neutrinoPx_realPart=0.0;
    m_neutrinoPy_realPart=0.0;
    m_neutrinoPz_realPart=0.0;
    m_neutrinoPx_rotation=0.0;
    m_neutrinoPy_rotation=0.0;
    m_neutrinoPz_rotation=0.0;
    m_neutrinoPx_fit.clear();
    m_neutrinoPy_fit.clear();
    m_neutrinoPz_fit.clear();
    
  }
  
  ///-- saveEvent - run for every systematic and every event --///
  void CustomEventSaver::saveEvent(const top::Event& event) 
  {
    bool truthLepPass=false;
    bool truthPhoPass=false;
    bool truthNuPass=false;
    
    ///-- set our variables to zero --///
    initEvent();
    bool isMC = top::isSimulation(event);
    int DSID=m_topconfig->getDSID();
    std::string xsFile = PathResolverFindCalibFile(m_topconfig->getTDPPath());
    auto sampleXsec = SampleXsectionSvc::svc(xsFile)->sampleXsection();
    m_xsec = sampleXsec->getXsection(DSID);
    m_kfactor = sampleXsec->getKfactor(DSID);
    if(isMC){
      top::check(evtStore()->retrieve (m_truthElectrons, m_topconfig->sgKeyTruthElectrons()), "Failed to retrieve truth Electrons");
      top::check(evtStore()->retrieve (m_truthMuons, m_topconfig->sgKeyTruthMuons()), "Failed to retrieve truth Muons");
      top::check(evtStore()->retrieve (m_truthPhotons, m_topconfig->sgKeyTruthPhotons()), "Failed to retrieve truth Photons");
      top::check(evtStore()->retrieve (m_truthTaus, m_topconfig->sgKeyTruthTaus()), "Failed to retrieve truth Taus");
      top::check(evtStore()->retrieve (m_truthJets, m_topconfig->sgKeyTruthJets()), "Failed to retrieve truth Jets");
      top::check(evtStore()->retrieve (m_truthNeutrinos, "TruthNeutrinos"), "Failed to retrieve truth Neutrinos");
    }
//const xAOD::EventInfo_v1 *eventInfo;
//top::check(evtStore()->retrieve( eventInfo, "EventInfo"),"failed!");
//std::cout<<"runnumber="<<eventInfo->runNumber()<<" ;lb="<<eventInfo->lumiBlock()<<" ;eventNumber="<<eventInfo->eventNumber()<<std::endl;
    static SG::AuxElement::Accessor<float> topoetcone40("topoetcone40");

    static SG::AuxElement::ConstAccessor<float> core57cellsEnergyCorrection("core57cellsEnergyCorrection");
    static SG::AuxElement::ConstAccessor<float> topoetconecoreConeSCEnergyCorrection("topoetconecoreConeSCEnergyCorrection");
    static SG::AuxElement::ConstAccessor<float> topoetcone40ptCorrection("topoetcone40ptCorrection");


    ////////////////
    ///get lepton///
    ////////////////
    static SG::AuxElement::Accessor<int> truthType("truthType");
    static SG::AuxElement::Accessor<int> truthOrigin("truthOrigin");
    static SG::AuxElement::Accessor<int> firstEgMotherTT("firstEgMotherTruthType");
    static SG::AuxElement::Accessor<int> firstEgMotherTO("firstEgMotherTruthOrigin");
    static SG::AuxElement::Accessor<unsigned char> ambiguityType("ambiguityType");
    static SG::AuxElement::Accessor<int> DFambiguityType("DFCommonAddAmbiguity");
    static SG::AuxElement::Accessor<char> ECIDS("DFCommonElectronsECIDS");
    static SG::AuxElement::Accessor<char> EG_Tight("DFCommonElectronsLHTight");
    std::vector<xAOD::IParticle::FourMom_t> lepton_p4={};
    double leaLep_pt=0.0;
    double subLep_pt=0.0;
    
    for(const xAOD::Electron* e : event.m_electrons) {
      if (e->pt() < 25000.) continue;
      float tmp_ePt=e->pt()/1000.0;
      particle_p4=e->p4();
      lepton_p4.push_back(particle_p4);

      if(tmp_ePt>leaLep_pt){
        subLep_p4=leaLep_p4;
        subLep_pt=leaLep_pt;

        leaLep_p4=particle_p4;
        leaLep_pt=tmp_ePt;
      }else if(tmp_ePt>subLep_pt){
        subLep_p4=particle_p4;
        subLep_pt=tmp_ePt;;
      }

      m_leptonPx.push_back (particle_p4.Px()*0.001);
      m_leptonPy.push_back (particle_p4.Py()*0.001);
      m_leptonPz.push_back (particle_p4.Pz()*0.001);
      m_leptonPt.push_back (e->pt ()*0.001);
      m_leptonE.push_back (particle_p4.E()*0.001);
      m_leptonEta.push_back (e->eta ());
      m_leptonPhi.push_back (e->phi ());
      
      if (e->charge() == -1) m_leptonID.push_back (-11);
      if (e->charge() == 1) m_leptonID.push_back (11);
      m_leptonPIDTight.push_back(EG_Tight(*e));
      m_leptonIsoTight.push_back(electronSelection.isIsoTight(*e));
      if(isMC) {
        m_leptonTruthTypeCondensed.push_back(electronSelection.truthTypeCondensed(*e));
      }
      m_leptonAmbiguity.push_back(ambiguityType(*e));
      m_leptonDFAmbiguity.push_back(DFambiguityType(*e));
      m_leptonECIDS.push_back(ECIDS(*e));
      //m_leptonTopoEt40.push_back(0.001*(topoetcone40(*e) + core57cellsEnergyCorrection(*e) + topoetcone40ptCorrection(*e) - topoetconecoreConeSCEnergyCorrection(*e)));
      m_Ht+=tmp_ePt;
      
      if(isMC){
        const xAOD::TruthParticle* associated_truth_electron = 0;
        float minDR_el = 0.1;
        for(const xAOD::TruthParticle* tele : *m_truthElectrons) {
          if(tele->pt() < 20000.) continue;
          float DR_val = tele->p4().DeltaR(e->p4());
          if (DR_val < minDR_el) {
            minDR_el = DR_val;
            associated_truth_electron = tele;
          }
        }
          
        for(const xAOD::TruthParticle* ttau : *m_truthTaus) {
          if(ttau->pt() < 20000.) continue;
          float DR_val = ttau->p4().DeltaR(e->p4());
          if (DR_val < minDR_el) {
            minDR_el = DR_val;
            associated_truth_electron = ttau;
          }
        }
        m_truthLeptonPx.push_back (associated_truth_electron ? associated_truth_electron->px()*0.001 : 0.0);
        m_truthLeptonPy.push_back (associated_truth_electron ? associated_truth_electron->py()*0.001 : 0.0);
        m_truthLeptonPz.push_back (associated_truth_electron ? associated_truth_electron->pz()*0.001 : 0.0);
        m_truthLeptonPt.push_back (associated_truth_electron ? associated_truth_electron->pt()*0.001 : 0.0);
        m_truthLeptonE.push_back (associated_truth_electron ? associated_truth_electron->e()*0.001 : 0.0);
        m_truthLeptonEta.push_back (associated_truth_electron ? associated_truth_electron->eta() : 0.0);
        m_truthLeptonPhi.push_back (associated_truth_electron ? associated_truth_electron->phi() : 0.0);
        m_truthLeptonDR.push_back (associated_truth_electron ? minDR_el : 99.0);
        m_truthLeptonID.push_back (associated_truth_electron ? associated_truth_electron->pdgId() : 0.0);
        if (associated_truth_electron) {
          auto truthParent = associated_truth_electron->parent();
          m_truthLeptonParent.push_back(truthParent ? truthParent->pdgId() : 0);
        } else {
          m_truthLeptonParent.push_back (0);
        }
      }
    }
    for(const xAOD::Muon* mu : event.m_muons) {
      if (mu->pt() < 25000.) continue;
      float tmp_muPt=mu->pt()/1000.0;
      particle_p4=mu->p4();
      lepton_p4.push_back(particle_p4);

      if(tmp_muPt>leaLep_pt){
        subLep_p4=leaLep_p4;
        subLep_pt=leaLep_pt;

        leaLep_p4=particle_p4;
        leaLep_pt=tmp_muPt;
        m_isEchannel=0;
      }else if(tmp_muPt>subLep_pt){
        subLep_p4=particle_p4;
        subLep_pt=tmp_muPt;;
      }

      m_leptonPx.push_back (particle_p4.Px()*0.001);
      m_leptonPy.push_back (particle_p4.Py()*0.001);
      m_leptonPz.push_back (particle_p4.Pz()*0.001);
      m_leptonPt.push_back (mu->pt ()*0.001);
      m_leptonE.push_back (particle_p4.E()*0.001);
      m_leptonEta.push_back (mu->eta ());
      m_leptonPhi.push_back (mu->phi ());

      if (mu->charge() == -1) m_leptonID.push_back (-13);
      if (mu->charge() == 1) m_leptonID.push_back (13);
      m_leptonPIDTight.push_back(muonSelection.isPIDTight(*mu));
      m_leptonIsoTight.push_back(muonSelection.isIsoTight(*mu));
      if(isMC) {
        m_leptonTruthTypeCondensed.push_back(muonSelection.truthTypeCondensed(*mu));
      }
      m_leptonAmbiguity.push_back(10);
      m_leptonDFAmbiguity.push_back(5);
      m_leptonECIDS.push_back(2);
      //m_leptonTopoEt40.push_back(0.001*(topoetcone40(*mu) + core57cellsEnergyCorrection(*mu) + topoetcone40ptCorrection(*mu) - topoetconecoreConeSCEnergyCorrection(*mu)));
      m_Ht+=tmp_muPt;
      
      if(isMC){
        const xAOD::TruthParticle* associated_truth_muon = 0;
        float minDR_mu = 0.1;
        for(const xAOD::TruthParticle* tmu : *m_truthMuons) {
          if(tmu->pt() < 20000.) continue;
          float DR_val = tmu->p4().DeltaR(mu->p4());
          if (DR_val < minDR_mu) {
            minDR_mu = DR_val;
            associated_truth_muon = tmu;
          }
        }
          
        for(const xAOD::TruthParticle* ttau : *m_truthTaus) {
          if(ttau->pt() < 20000.) continue;
          float DR_val = ttau->p4().DeltaR(mu->p4());
          if (DR_val < minDR_mu) {
            minDR_mu = DR_val;
            associated_truth_muon = ttau;
          }
        }
        m_truthLeptonPx.push_back (associated_truth_muon ? associated_truth_muon->px()*0.001 : 0.0);
        m_truthLeptonPy.push_back (associated_truth_muon ? associated_truth_muon->py()*0.001 : 0.0);
        m_truthLeptonPz.push_back (associated_truth_muon ? associated_truth_muon->pz()*0.001 : 0.0);
        m_truthLeptonPt.push_back (associated_truth_muon ? associated_truth_muon->pt()*0.001 : 0.0);
        m_truthLeptonE.push_back (associated_truth_muon ? associated_truth_muon->e()*0.001 : 0.0);
        m_truthLeptonEta.push_back (associated_truth_muon ? associated_truth_muon->eta() : 0.0);
        m_truthLeptonPhi.push_back (associated_truth_muon ? associated_truth_muon->phi() : 0.0);
        m_truthLeptonDR.push_back (associated_truth_muon ? minDR_mu : 99.0);
        m_truthLeptonID.push_back (associated_truth_muon ? associated_truth_muon->pdgId() : 0.0);
        if (associated_truth_muon) {
          auto truthParent = associated_truth_muon->parent();
          m_truthLeptonParent.push_back(truthParent ? truthParent->pdgId() : 0);
        } else {
          m_truthLeptonParent.push_back (0);
        }
      }
    }

    if(isMC){  
      //Finding the leading truth lepton
      float minDR_truthLep = 0.1;
      for (const xAOD::TruthParticle* te : *m_truthElectrons) {
        if(te->pt() < 17000.) continue;
        float DR_val = te->p4().DeltaR(leaLep_p4);
        if (DR_val < minDR_truthLep) {
          minDR_truthLep = DR_val;
          truthLeaLep_p4 = te->p4();
        }
      }

      for (const xAOD::TruthParticle* tm : *m_truthMuons) {
        if(tm->pt() < 17000.) continue;
        float DR_val = tm->p4().DeltaR(leaLep_p4);
        if (DR_val < minDR_truthLep) {
          minDR_truthLep = DR_val;
          truthLeaLep_p4 = tm->p4();
        }
      }
      for (const xAOD::TruthParticle* tt : *m_truthTaus) {
        if(tt->pt() < 17000.) continue;
        float DR_val = tt->p4().DeltaR(leaLep_p4);
        if (DR_val < minDR_truthLep) {
          minDR_truthLep = DR_val;
          truthLeaLep_p4 = tt->p4();
        }
      }
      truthLepPass = minDR_truthLep < 0.1;
        
      float minDR_nu = 99.;
      for(const xAOD::TruthParticle* tnu : *m_truthNeutrinos) {
        if(tnu->pt() < 10000.) continue;
        float DR_val = truthLeaLep_p4.DeltaR(tnu->p4());
        if(DR_val < minDR_nu) {
          minDR_nu = DR_val;
          truthClosNu_p4 = tnu->p4();
        }
        m_truthNeutrinoPx.push_back (tnu->px()*0.001);
        m_truthNeutrinoPy.push_back (tnu->py()*0.001);
        m_truthNeutrinoPz.push_back (tnu->pz()*0.001);
        m_truthNeutrinoPt.push_back (tnu->pt()*0.001);
        m_truthNeutrinoE.push_back (tnu->e()*0.001);
        m_truthNeutrinoPhi.push_back (tnu->phi());
        m_truthNeutrinoEta.push_back (tnu->eta());
        m_truthNeutrinoDR.push_back (DR_val);
        auto truthParent = tnu->parent();
        m_truthNeutrinoParent.push_back (truthParent ? truthParent->pdgId() : 0);
      }
      truthNuPass = minDR_nu < 10.;
    }
    ////////////////
    ///get photon///
    ////////////////
    static SG::AuxElement::Accessor<char> PH_Tight("DFCommonPhotonsIsEMTight");
    static SG::AuxElement::Accessor<unsigned int> isEMTight("DFCommonPhotonsIsEMTightIsEMValue");

    double closPho_dR=10.0;
    int closPho_iter=0;
    for(const xAOD::Photon* pho : event.m_photons){
      if (pho->pt() < 25000.) continue;
      m_numPho++;
      float dR = leaLep_p4.DeltaR(pho->p4());
      if(dR<closPho_dR){
        closPho_dR=dR;
        closPho_p4=pho->p4();
        m_closPhoPosition=closPho_iter;
      }
      particle_p4=pho->p4();
      closPho_iter++;
      m_photonPx.push_back (particle_p4.Px()*0.001);
      m_photonPy.push_back (particle_p4.Py()*0.001);
      m_photonPz.push_back (particle_p4.Pz()*0.001);
      m_photonPt.push_back (pho->pt()*0.001);
      m_photonE.push_back (particle_p4.E()*0.001);
      m_photonPhi.push_back (pho->phi ());
      m_photonEta.push_back (pho->eta ());
      m_photonDR.push_back (dR);
      m_photonPIDTight.push_back (PH_Tight(*pho));
      m_photonIsoTight.push_back(photonSelection.isIsoTight(*pho));
      m_photonPassLoosePrime2.push_back(photonSelection.passLoosePrime(*pho,2));
      m_photonPassLoosePrime3.push_back(photonSelection.passLoosePrime(*pho,3));
      m_photonPassLoosePrime4.push_back(photonSelection.passLoosePrime(*pho,4));
      m_photonPassLoosePrime4a.push_back(photonSelection.passLoosePrime(*pho,41));
      m_photonPassLoosePrime5.push_back(photonSelection.passLoosePrime(*pho,5));
      
      
      if(isMC) m_photonTruthTypeCondensed.push_back(photonSelection.truthTypeCondensed(truthType(*pho),truthOrigin(*pho)));

      m_topoEt40.push_back(0.001*(topoetcone40(*pho)));
      m_topoEt40Corrected.push_back(0.001*(topoetcone40(*pho) + core57cellsEnergyCorrection(*pho) + topoetcone40ptCorrection(*pho) - topoetconecoreConeSCEnergyCorrection(*pho)));
      m_Ht+=pho->pt()*0.001;
        
      if(isMC){  
        const xAOD::TruthParticle* associated_truth_photon = 0;
        float minDR_pho = 0.1;
        for(const xAOD::TruthParticle* tpho : *m_truthPhotons) {
          if(tpho->pt() < 17000.) continue;
          float DR_val = tpho->p4().DeltaR(pho->p4());
          if (DR_val < minDR_pho) {
            minDR_pho = DR_val;
            associated_truth_photon = tpho;
          }
        }
        m_truthPhotonPx.push_back (associated_truth_photon ? associated_truth_photon->px()*0.001 : 0.0);
        m_truthPhotonPy.push_back (associated_truth_photon ? associated_truth_photon->py()*0.001 : 0.0);
        m_truthPhotonPz.push_back (associated_truth_photon ? associated_truth_photon->pz()*0.001 : 0.0);
        m_truthPhotonPt.push_back (associated_truth_photon ? associated_truth_photon->pt()*0.001 : 0.0);
        m_truthPhotonE.push_back (associated_truth_photon ? associated_truth_photon->e()*0.001 : 0.0);
        m_truthPhotonEta.push_back (associated_truth_photon ? associated_truth_photon->eta() : 0.0);
        m_truthPhotonPhi.push_back (associated_truth_photon ? associated_truth_photon->phi() : 0.0);
        m_truthPhotonDR.push_back (associated_truth_photon ? minDR_pho : 99.0);
        if (associated_truth_photon) {
          auto truthParent = associated_truth_photon->parent();
          m_truthPhotonParent.push_back(truthParent ? truthParent->pdgId() : 0);
        } else {
          m_truthPhotonParent.push_back (0);
        }
      }
    }
    bool phoPass = (closPho_dR < 10.);
      
    if(isMC){
      //Associated truth photon to the closest photon to the leading lepton
      float minDR_truthPho = 0.15;
      for(const xAOD::TruthParticle* tp : *m_truthPhotons) {
        if(tp->pt() < 20000.) continue;
        float DR_val = tp->p4().DeltaR(closPho_p4);
        if (DR_val < minDR_truthPho) {
          minDR_truthPho = DR_val;
          truthClosPho_p4 = tp->p4();
        }
      }
      truthPhoPass = minDR_truthPho < 0.1;
    }
    
    //////////////
    ///get jets///
    //////////////
    for(const xAOD::Jet* jet : event.m_jets){
      m_jetPt.push_back(jet->pt()*0.001);
      m_jetPhi.push_back(jet->phi());
      m_jetEta.push_back(jet->eta());
      m_Ht+=jet->pt()*0.001;
        
      if(isMC){  
        const xAOD::Jet* associated_truth_jet = 0;
        float minDR_jet = 0.1;
        for(const xAOD::Jet* tjet : *m_truthJets) {
          if(tjet->pt() < 20000.) continue;
          float DR_val = tjet->p4().DeltaR(jet->p4());
          if (DR_val < minDR_jet) {
            minDR_jet = DR_val;
            associated_truth_jet = tjet;
          }
        }
        m_truthJetPx.push_back (associated_truth_jet ? associated_truth_jet->px()*0.001 : 0.0);
        m_truthJetPy.push_back (associated_truth_jet ? associated_truth_jet->py()*0.001 : 0.0);
        m_truthJetPz.push_back (associated_truth_jet ? associated_truth_jet->pz()*0.001 : 0.0);
        m_truthJetPt.push_back (associated_truth_jet ? associated_truth_jet->pt()*0.001 : 0.0);
        m_truthJetE.push_back (associated_truth_jet ? associated_truth_jet->e()*0.001 : 0.0);
        m_truthJetEta.push_back (associated_truth_jet ? associated_truth_jet->eta() : 0.0);
        m_truthJetPhi.push_back (associated_truth_jet ? associated_truth_jet->phi() : 0.0);
        m_truthJetDR.push_back (associated_truth_jet ? minDR_jet : 99.0);
      }
    }
    
    
    /////////////////////////////////
    ///Other info based on objects///
    /////////////////////////////////
      const xAOD::MissingET* met=event.m_met;
      m_METx=met->mpx()*0.001;
      m_METy=met->mpy()*0.001;
      m_MET=sqrt(pow(m_METx,2)+pow(m_METy,2));

    
    float lep_Et = sqrt(pow(leaLep_p4.M(),2)+pow(leaLep_p4.Pt(),2))*0.001;
    if(phoPass){
      m_ptlgMET=quadAdd(m_METx+leaLep_p4.Px()*0.001+closPho_p4.Px()*0.001, m_METy+leaLep_p4.Py()*0.001+closPho_p4.Py()*0.001);
      m_pq=leaLep_p4.Dot(closPho_p4)*0.001*0.001;//to convert to GeV^2, time 10^-3 twice 
      lepPho_p4=leaLep_p4+closPho_p4;
      m_invmlg=lepPho_p4.M()*0.001;
      
      m_mtlg = sqrt(pow(lep_Et+closPho_p4.Pt()*0.001,2) - pow(lepPho_p4.Px()*0.001,2) - pow(lepPho_p4.Py()*0.001,2));
      m_mtlmet = sqrt(pow(lep_Et+m_MET,2) - pow(leaLep_p4.Px()*0.001+m_METx,2) - pow(leaLep_p4.Py()*0.001+m_METy,2));
      m_mtlgmet = sqrt(pow(lep_Et+closPho_p4.Pt()*0.001+m_MET,2) - pow(lepPho_p4.Px()*0.001+m_METx,2) - pow(lepPho_p4.Py()*0.001+m_METy,2));
    }
 
    if(m_leptonPt.size()>1){
        auto lepCombined_p4=leaLep_p4+subLep_p4;
        m_ptllMET=quadAdd(m_METx+lepCombined_p4.Px()*0.001, m_METy+lepCombined_p4.Py()*0.001);
        m_invmll=(leaLep_p4+subLep_p4).M()*0.001;
        auto subLepEt= sqrt(pow(subLep_p4.M(),2)+pow(subLep_p4.Pt(),2))*0.001;
        m_mtllmet = sqrt(pow(lep_Et+subLepEt+m_MET,2) - pow(lepCombined_p4.Px()*0.001+m_METx,2) - pow(lepCombined_p4.Py()*0.001+m_METy,2));
    } 

//Calculate the invariant mass of Wy
    if(m_leptonPt.size()>0 && m_photonPt.size()>0){
      TLorentzVector lepton(leaLep_p4.Px()*0.001,leaLep_p4.Py()*0.001,leaLep_p4.Pz()*0.001,leaLep_p4.E()*0.001);
      TLorentzVector tmp_neutrino(m_METx,m_METy,0.0,m_MET);
      TLorentzVector closPhoton(closPho_p4.Px()*0.001,closPho_p4.Py()*0.001,closPho_p4.Pz()*0.001,closPho_p4.E()*0.001);

      std::vector<TLorentzVector *> realPartOutput= NeutrinoBuilder.candidatesFromWMass_RealPart(&lepton, tmp_neutrino.Pt(), tmp_neutrino.Phi(), true,80.4);
      std::vector<TLorentzVector *> rotationOutput= NeutrinoBuilder.candidatesFromWMass_Rotation(&lepton, tmp_neutrino.Pt(), tmp_neutrino.Phi(), true);


      TLorentzVector neutrino_realPart = (*realPartOutput.at(0));
      TLorentzVector neutrino_rotation = (*rotationOutput.at(0));
  
      m_invmW_realPart=(lepton + neutrino_realPart).M();
      m_invmWy_realPart=(lepton + neutrino_realPart + closPhoton).M();    
      m_invmW_rotation=(lepton + neutrino_rotation).M();
      m_invmWy_rotation=(lepton + neutrino_rotation + closPhoton).M();
      
      m_neutrinoPx_realPart=neutrino_realPart.Px();
      m_neutrinoPy_realPart=neutrino_realPart.Py();
      m_neutrinoPz_realPart=neutrino_realPart.Pz();
      m_neutrinoPx_rotation=neutrino_rotation.Px();
      m_neutrinoPy_rotation=neutrino_rotation.Py();
      m_neutrinoPz_rotation=neutrino_rotation.Pz();

      for (int cx=0;cx<m_cxList.size();cx++){
        for (int cz=0;cz<m_czList.size();cz++){
          TLorentzVector neutrino_postFit = NeutrinoBuilder.candidatesFromWMass_fit(&lepton, m_MET, tmp_neutrino.Phi(),m_cxList.at(cx),m_cxList.at(cx),m_czList.at(cz),1).at(0);

          m_invmW_fit.push_back((lepton + neutrino_postFit).M());
          m_invmWy_fit.push_back((lepton +  neutrino_postFit + closPhoton).M());

          m_neutrinoPx_fit.push_back(neutrino_postFit.Px());
          m_neutrinoPy_fit.push_back(neutrino_postFit.Py());
          m_neutrinoPz_fit.push_back(neutrino_postFit.Pz());
        }
      }


    }else if (m_leptonPt.size()>1 && m_photonPt.size()==0){
      //Fake photon from electron:
      TLorentzVector lepton(leaLep_p4.Px()*0.001,leaLep_p4.Py()*0.001,leaLep_p4.Pz()*0.001,leaLep_p4.E()*0.001);
      TLorentzVector tmp_neutrino(m_METx,m_METy,0.0,m_MET);
      TLorentzVector closPhoton(subLep_p4.Px()*0.001,subLep_p4.Py()*0.001,subLep_p4.Pz()*0.001,subLep_p4.E()*0.001);

      std::vector<TLorentzVector *> realPartOutput= NeutrinoBuilder.candidatesFromWMass_RealPart(&lepton, tmp_neutrino.Pt(), tmp_neutrino.Phi(), true,80.4);
      std::vector<TLorentzVector *> rotationOutput= NeutrinoBuilder.candidatesFromWMass_Rotation(&lepton, tmp_neutrino.Pt(), tmp_neutrino.Phi(), true);

      TLorentzVector neutrino_realPart = (*realPartOutput.at(0));
      TLorentzVector neutrino_rotation = (*rotationOutput.at(0));
      
      m_neutrinoPx_realPart=neutrino_realPart.Px();
      m_neutrinoPy_realPart=neutrino_realPart.Py();
      m_neutrinoPz_realPart=neutrino_realPart.Pz();
      m_neutrinoPx_rotation=neutrino_rotation.Px();
      m_neutrinoPy_rotation=neutrino_rotation.Py();
      m_neutrinoPz_rotation=neutrino_rotation.Pz();

      m_invmW_realPart=(lepton + neutrino_realPart).M();
      m_invmWy_realPart=(lepton + neutrino_realPart + closPhoton).M();
      m_invmW_rotation=(lepton + neutrino_rotation).M();
      m_invmWy_rotation=(lepton + neutrino_rotation + closPhoton).M();
      

      for (int cx=0;cx<m_cxList.size();cx++){
        for (int cz=0;cz<m_czList.size();cz++){
          TLorentzVector neutrino_postFit = NeutrinoBuilder.candidatesFromWMass_fit(&lepton, m_MET, tmp_neutrino.Phi(),m_cxList.at(cx),m_cxList.at(cx),m_czList.at(cz),1).at(0);

          m_invmW_fit.push_back((lepton + neutrino_postFit).M());
          m_invmWy_fit.push_back((lepton +  neutrino_postFit + closPhoton).M());

          m_neutrinoPx_fit.push_back(neutrino_postFit.Px());
          m_neutrinoPy_fit.push_back(neutrino_postFit.Py());
          m_neutrinoPz_fit.push_back(neutrino_postFit.Pz());
        }
      }

    }


      
      if(isMC){
        m_passVyOR = MCoverlapRemoval.checkOR(isMC, DSID, m_truthPhotons, m_truthElectrons, m_truthMuons);
        if(truthLepPass && truthPhoPass) {
          truthLepPho_p4 = truthLeaLep_p4+truthClosPho_p4;
          m_invmtruthlg = truthLepPho_p4.M()*0.001;
          if(truthNuPass) {
            truthLepNu_p4 = truthLeaLep_p4+truthClosNu_p4;
            truthLepPhoNu_p4 = truthLepPho_p4+truthClosNu_p4;

            m_invmtruthlnu = truthLepNu_p4.M()*0.001;
            m_invmtruthlgnu = truthLepPhoNu_p4.M()*0.001;
          }
        }
      }

      //
      top::EventSaverFlatNtuple::saveEvent(event);
    
    
  }
  
  double CustomEventSaver::quadAdd(double x, double y)
  {
    return sqrt(x*x+y*y);
  }

}
