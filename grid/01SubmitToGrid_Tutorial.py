#!/usr/bin/env python

# Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
import TopExamples.grid
import DerivationTags
import Data_rel21
import MC16_PHYS

config = TopExamples.grid.Config()
config.code          = 'top-xaod'
config.settingsFile  = 'HowtoExtendAnalysisTop/custom-saver-test.txt'
config.gridUsername  = 'upatel'
config.suffix        = '220929'
config.excludedSites = ''
config.noSubmit      = False # set to True if you just want to test the submission
config.CMake         = True # need to set to True for CMake-based releases (release 21)
config.mergeType     = 'Default' #'None', 'Default' or 'xAOD'
config.destSE        = '' #This is the default (anywhere), or try e.g. 'UKI-SOUTHGRID-BHAM-HEP_LOCALGROUPDISK'
config.otherOptions  = '--maxNFilesPerJob 4'

###Data - look in Data_rel21.py
###MC Simulation - look in MC16_PHYS.py
###Using a few test samples produced with release 21
###Edit these lines if you don't want to run everything!
names = [
#MC16a
    #'ttbar/PHYSa',
    #'yjets/PHYSa',
    #'DiPhoton/PHYSa',
    #'EWKVy/PHYSa',
    #'Vyy/PHYSa',
    #'VVy/PHYSa',
    #'Wy/PHYSa',
    #'Zy/PHYSa',
    #'tty/PHYSa',
    #'VV/PHYSa',
    #'signal/PHYSa',
    #'Data15/PHYS',
    #'Data16/PHYS',
    #'Wjets/PHYSa'
    #'retry',
    #'DataRetry'
    #'newWjets/PHYSa'
    'newZjets/PHYSa'

    #MC16d
#In local XSection File
    #'Wy/PHYSd',
    #'Zy/PHYSd',
    #'tty/PHYSd',
    #'ttbar/PHYSd',
    #'signal/PHYSd',
    #'yjets/PHYSd',
    #'DiPhoton/PHYSd',
    #'EWKVy/PHYSd',
    #'Vyy/PHYSd',
    #'VVy/PHYSd',
    #'SingleTop/PHYSd',
#Not in local XSection File
    #'Wjets/PHYSd',
    #'Zjets/PHYSd',
#Data
    #'Data17/PHYS',
#MC16e
#In local XSection File
#    'Wy/PHYSe',
#    'Zy/PHYSe',
#    'tty/PHYSe',
#    'ttbar/PHYSe',
#    'signal/PHYSe',
#    'yjets/PHYSe',
#    'DiPhoton/PHYSe',
    #'EWKVy/PHYSe',
#    'Vyy/PHYSe',
#    'VVy/PHYSe',
    #'SingleTop/PHYSe',
#Not in local XSection File
#    'Wjets/PHYSe',
#    'Zjets/PHYSe',
#Data
    #'Data18/PHYS',

]

samples = TopExamples.grid.Samples(names)
TopExamples.grid.submit(config, samples)

